((c-mode . ((flycheck-clang-include-path "." ".." "/usr/include/libevdev-1.0"
                                         "/usr/include/guile/2.2")
            (flycheck-gcc-include-path "." ".." "/usr/include/libevdev-1.0"
                                       "/usr/include/guile/2.2")
            (flycheck-cppcheck-include-path "." ".." "/usr/include/libevdev-1.0"
                                       "/usr/include/guile/2.2")
            (flycheck-clang-language-standard . "c99")
            (flycheck-gcc-language-standard . "c99")
            (flycheck-cppcheck-language-standard "c99"))))
