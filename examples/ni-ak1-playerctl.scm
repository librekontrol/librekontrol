(use-modules (librekontrol core)
             (librekontrol device)
             (librekontrol input)
             (librekontrol alsa)
             ((librekontrol devices ni-ak1) #:prefix ak1:)
             (ice-9 popen)
             (ice-9 rdelim)
             (ice-9 format))

(define (playerctl-playing?)
  (let* ((playerctl-port (open-input-pipe "playerctl status"))
         (playerctl-status (read-line playerctl-port)))
    (close-pipe playerctl-port)
    (equal? playerctl-status "Playing")))

(configure-device
 #:alsa-name ak1:alsa-name
 #:input-name ak1:input-name
 #:controls (list (list ak1:left system-button
                        #:cmd "playerctl previous")
                  (list ak1:middle (make-system-button-toggle
                                    (playerctl-playing?))
                        #:on-cmd "playerctl play"
                        #:off-cmd "playerctl pause")
                  (list ak1:right system-button
                        #:cmd "playerctl next")
                  (list ak1:knob (make-abs-to-rel-system-knob)
                        #:cmdproc (lambda (x)
                                    (format #f "playerctl position ~d~:[-~;+~]"
                                            (abs x) (> x 0)))
                        #:knob-max (ak1:knob-max)
                        #:invert #t))
 #:idle-wait 0.5
 #:idle-hooks (list (lambda ()
                      (set-ctl (control-alsa-ctl ak1:knob) #f))
                    (lambda ()
                      (set-ctl (control-alsa-ctl ak1:middle) (playerctl-playing?))))
 #:exit-hooks (list (lambda ()
                      (turn-off-ctls
                       (map control-alsa-ctl
                            (list ak1:left ak1:middle ak1:right ak1:knob)))))
 #:init (list (lambda ()
                (set-ctl (control-alsa-ctl ak1:middle) (playerctl-playing?)))))


