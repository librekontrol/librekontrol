2025-03-06  Brandon Invergo  <brandon@invergo.net>

	* doc/librekontrol.texi: Fix outdated MIDI note symbols.

	* guile/librekontrol/midi.scm: Fix another error in constructing
	MIDI note names.

2023-11-28  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/midi.scm (construct-note-name): Fix a silly
	arithmetic error in constructing MIDI note names.

2021-03-26  Brandon Invergo  <brandon@invergo.net>

	* src/lk-device.c (set_boolean_ctl,set_integer_ctl): Add some
	missing mutex unlocking.

2021-01-06  Brandon Invergo  <brandon@invergo.net>

	* src/lk-device.c: Avoid re-writing the same value to ALSA ctl's
	repeatedly.  This can happen when rescaling large input values
	down to a narrower LED brightness range.  The result is an
	improvement in performance in some cases because changing LED
	brightness is slow.

2021-01-03  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm: Resolve an innocuous compilation
	error in connect-control.


2020-04-20  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/devices/ni-rk2.scm: Fix the Rig Kontrol 2
	ALSA Ctl definitions.

	* guile/librekontrol/devices/ni-rk3.scm: Fix the Rig Kontrol 3
	ALSA Ctl definitions and reorder them to match the labeling on the
	device (previously they were labeled according to the Linux caiaq
	device driver code's specification).

2020-04-03  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/devices/ni-rk2.scm: Fix the names
	of Rig Kontrol 2 inputs / controls on the device's back panel

	* guile/librekontrol/devices/ni-rk3.scm: Fix the names
	of Rig Kontrol 3 inputs / controls on the device's back panel

2020-03-31  Brandon Invergo  <brandon@invergo.net>

	* src/lk-device.c (finalize_remap_dev,send_remap_event): Convert
	the error code returned by libevdev functions to errno in order to
	print out more useful errors.

	* guile/librekontrol/device.scm: For any callback function that
	calls system processes, construct the shell command from a
	procedure instead of taking the string directly.  This is more
	flexible.

	* guile/librekontrol/device.scm (configure-device): Init
	procedures were not being called correctly.  Now they are.

	* examples/ni-ak1-playerctl.scm: The NI Audio Kontrol1 example for
	controlling music via playerctl has been improved.

2019-09-29  Brandon Invergo  <brandon@invergo.net>

	* src/librekontrol-core.c: Use a more C99-like style

	* src/librekontrol.c: Use a more C99-like style

	* src/lk-device.c: Use a more C99-like style

2019-07-09  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm (configure-device): Force the idle
	hooks to be cleared before running any exit hooks (just in case
	the idle timer hits before the exit hooks have finished running)

	* src/librekontrol-core.c (lk_scm_reset_exit_hook)
	(lk_scm_reset_idle_hook): remove unnecessary/unused argument

2019-07-08  Brandon Invergo  <brandon@invergo.net>

	* src/librekontrol-core.c (get_exit_handler): Fix failure when
	trying to add a procedure to an existing exit hook.
	(get_idle_handler): Fix failure when trying to add a procedure to
	an existing idle hook.

2019-06-18  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm
	(make-finite-midi-control-abs-knob): Add procedure to create
	callback functions for infinite rotary encoders to behave like
	finite MIDI controls.

	* guile/librekontrol/device.scm (midi-control-abs-knob): Add a
	check to ensure that alsa-ctl is available before trying to set
	it.

	* guile/librekontrol/alsa.scm (set-ctl): Add a check to ensure
	that alsa-ctl is available before trying to get its value.

2019-04-11  Brandon Invergo  <brandon@invergo.net>

	* src/lk-device.c (open_device): Allow opening a MIDI sequencer
	for a device even if it isn't known to ALSA.

2019-04-01  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm (make-remap-button-toggle): Don't
	"hold down" the button when toggling a remap-button-toggle unless
	an option is set.

2019-03-28  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm
	(remap-button,make-remap-button-toggle): Support modifier keys for
	remap-button callbacks.  Don't ask for the output event-type;
	assume that only keypress events are being sent.
	(rel-knob-to-button,make-abs-knob-to-button): Support sending
	keypress events from rel/abs controls.

2019-03-27  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/input.scm: (make-input-event): Break out the
	input-event making from input-event symbol definition.

	* guile/librekontrol/device.scm (configure-device): Replace
	'define-device' macro with 'configure-device' function.
	(make-abs-to-rel-system-knob): Fix conversion of absolute
	positions to relative positions.  Also account for "infinite
	rotary"-style knobs, where the absolute position wraps around.

2019-03-24  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm: Rename "switch"-making procedures
	to "toggle"-making procedures.  Add more knob-related callbacks.

2019-03-20  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm: Allow procedures that make
	button-switch callbacks to accept an optional initial state
	(initially on or off).

	* src/librekontrol-core.c: Move all midi requests outside of
	Guile as with ALSA control requests.

2019-03-19  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm (make-input-max-parameter): Add
	helper function to carefully parameterize device input max values.

	* src/librekontrol.c (run_exit_hooks): Be sure to run Scheme
	cleanup code (closing devices and controls) when handling signals.

	* guile/librekontrol/alsa.scm (set-ctl,toggle-ctl): Move set-ctl
	and toggle-ctl to alsa.scm
	(turn-off-ctls): Add helper macro to turn off all ctls in a list

	* guile/librekontrol/device.scm (define-device): Add #:init
	section

2019-03-18  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm (define-device): Complete rewrite
	of the define-device macro to be more flexible.

2019-03-17  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm: Add functions to make "switch"
	versions of the button callbacks, which remember their state via
	lexical closure.

2019-03-16  Brandon Invergo  <brandon@invergo.net>

	* src/librekontrol-core.c (get_idle_wait): Assume that idle-wait
	is in seconds, to be converted to microseconds upon request.

2019-03-14  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/devices/ni-maschine.scm:
	* guile/librekontrol/devices/ni-rk2.scm:
	* guile/librekontrol/devices/ni-rk3.scm: Add control definitions
	for more devices.

2019-03-10  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm (define-device): Add the initial
	version of the big define-device macro.

2019-03-07  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm (midi-note-pad): Add a MIDI
	callback function for pads and other velocity-sensitive inputs.

2019-03-06  Brandon Invergo  <brandon@invergo.net>

	* guile/librekontrol/device.scm
	(midi-note-button,remap-button,system-button,midi-control-knob):
	Add several high-level callback functions.

	* guile/librekontrol/midi.scm (midi-controls): Add MIDI control
	definitions

	* src/lk-device.c (send_midi_control): Add send_midi_control
	function.

	* src/librekontrol-core.c (lk_scm_send_midi_control): Add
	send-midi-control procedure.

	* src/lk-device.c: Rename ni-device.c to lk-device.c

	* src/librekontrol-core.c: Rename guile-ni-device.c to
	librekontrol-core.c

2019-02-25  Brandon Invergo  <brandon@invergo.net>

	* src/guile-ni-device.c (lk_scm_open_device,lk_scm_open_ctl):
	Rename low-level opening functions.

2019-02-21  Brandon M. Invergo  <brandon@invergo.net>

	* src/devices/*: Move all device definitions out of C and into Guile

2019-02-19  Brandon M. Invergo  <brandon@invergo.net>

	* guile/librekontrol/midi.scm: Move MIDI note definitions from
	guile-midi.c out to Scheme (and remove the C code)

	* guile/librekontrol/input-codes.scm: Use `define-public` instead
	of having a massive export block

2019-02-18  Brandon M. Invergo  <brandon@invergo.net>

	* guile/librekontrol/input-codes.scm: Add input codes Scheme module

	* src/guile-ni-device.c: Add evdev remap functions to Guile

	* src/ni-device.c: Rename "output" evdev connections to "remap",
	i.e. "input remapper"

2019-02-13  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c
	(open_device, output_enable_event, output_disable_event)
	(output_set_abs_info, finalize_output_dev, send_output_event): Add
	option to open a uinput device for remapping input events.

2019-02-12  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c (open_device)
	* src/guile-ni-device.c (lk_scm_open_ni_device)
	(open_device_wrapper): Allow input-only devices and make opening a
	MIDI seq optional
	(run_exit_hook,get_idle_wait,run_idle_hook,handle_event): Add
	const hints to function parameters.

	* src/librekontrol.c (main): Make debug output optional

2019-02-11  Brandon Invergo  <brandon@invergo.net>

	* src/guile-ni-device.c: Migrate from SMOBs to foreign-objects,
	move all ALSA (etc) memory handling outside of Guile's remit.
	* src/librekontrol.c: Update to handle new handling of ALSA (etc)
	memory.
	* src/devices/*.c: Fixes for Guile 2.2.4 API changes.

2016-12-08  Brandon Invergo  <brandon@invergo.net>

	* src/guile-ni-device.c: Add code for idle and exit handlers.
	* src/librekontrol.c: Add code for idle and exit handlers.

2016-12-01  gettextize  <bug-gnu-gettext@gnu.org>

	* m4/gettext.m4: New file, from gettext-0.19.8.1.
	* m4/iconv.m4: New file, from gettext-0.19.8.1.
	* m4/lib-ld.m4: New file, from gettext-0.19.8.1.
	* m4/lib-link.m4: New file, from gettext-0.19.8.1.
	* m4/lib-prefix.m4: New file, from gettext-0.19.8.1.
	* m4/nls.m4: New file, from gettext-0.19.8.1.
	* m4/po.m4: New file, from gettext-0.19.8.1.
	* m4/progtest.m4: New file, from gettext-0.19.8.1.
	* Makefile.am (SUBDIRS): Add po.
	* configure.ac (AC_CONFIG_FILES): Add po/Makefile.in.

2016-11-15  Brandon Invergo  <brandon@invergo.net>

	* src/devices/kontrols4.c: Add KontrolS4 definitions.

	* src/guile-devices.c (load_device_defs): Load device definitions
	from individual files.

2016-10-24  Brandon Invergo  <brandon@invergo.net>

	* src/guile-ni-device.c (lk_scm_open_ni_device): Allow for a NULL
	input (for devices without any inputs).

	* src/ni-device.c (open_device): Allow for a NULL input (for
	devices without any inputs).

2016-10-23  Brandon Invergo  <brandon@invergo.net>

	* src/guile-devices.c (load_maschine_defs): Add missing
	definitions.
	(load_rk2_defs): Add RigKontrol2 definitions.
	(load_rk3_defs): Add RigKontrol3 definitions.
	(load_kore_defs): Add Kore Cortroller definitions.

2016-10-19  Brandon Invergo  <brandon@invergo.net>

	* src/librekontrol.c: Implement checking for the default init file
	in a standard location and support loading an alternative file.

	* src/guile-ni-device.c: Add checks for unopened devices/controls;
	(lk_scm_{add,remove,reset}_event_hook): Replace map-event with a
	Scheme hook-based solution, adding the add-event-hook!,
	remove-event-hook! and reset-event-hook! functions.

	* src/ni-device.c: Add checks for NULL pointers throughout.

2016-10-16  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c, src/guile-ni-device.c: Be more lenient about
	errors when opening an ALSA device/control, and be sure to handle
	those errors in Guile.

2016-10-12  Brandon Invergo  <brandon@invergo.net>

	* src/guile-ni-device.c
	(lk_scm_integer_ctl_max,lk_scm_integer_ctl_min): Add functions
	to get min/max integer ctl values.

	* src/ni-device.c (integer_ctl_min,integer_ctl_max): Add functions
	to get min/max integer ctl values.

2016-10-11  Brandon Invergo  <brandon@invergo.net>

	* src/guile-devices.c (load_device_defs): Add functions for
	defining machine-specific values.

2016-10-09  Brandon Invergo  <brandon@invergo.net>

	* src/guile-ni-device.c
	(add_device,get_device_list,get_num_devices,handle_event): Add
	more functions to properly define devices and handle events.

2016-10-08  Brandon Invergo  <brandon@invergo.net>

	* src/guile-ni-device.c: Add initial Guile interface.

2016-10-07  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c (get_device_event,device_has_event_pending):
	Move event handling stuff entirely to the main loop.

2016-10-06  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c (get_device_event,device_has_event_pending):
	Device parameter should be a pointer.
	(open_control,set/get_*_ctl): Add a device pointer to the
	ni_device_ctl structure, removing the need to pass it separately
	to functions.

2016-10-03  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c (grab_input_dev): Add function to find the evdev
	event file for the device and open it.
	(open_device): New argument: input device name (usually different
	from the ALSA controller name).  Refactor to split the various
	tasks into separate functions.
	(get_device_event, device_has_event_pending): Add event
	handling code.

2016-10-02  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c (send_midi_*): Add functions for sending MIDI
	events.
	(open_device): Add sequencer handle to device structure.

2016-07-09  Brandon Invergo  <brandon@invergo.net>

	* src/ni-device.c: Reorganize all functions to use device and
	control structs, and provide functions (open_device/close_device &
	open_control/close_control) to work with them.
	(set_integer_ctl, get_integer_ctl): Add functions to get and set
	integer control values
	(set_boolean_ctl, get_boolean_ctl): Add functions to get and set
	boolean values
	(toggle_boolean_ctl): Simplify toggle_boolean_ctl to use the set
	and get functions

2016-07-07  Brandon Invergo  <brandon@invergo.net>

	* src/alsa-device.c: Add initial code for toggling boolean
	controls.

