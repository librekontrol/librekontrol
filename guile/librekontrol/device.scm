;; Copyright (C) 2019, 2020  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol device)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 optargs)
  #:use-module (librekontrol core)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol input)
  #:use-module ((librekontrol midi) #:prefix midi:)
  #:export (make-control
            control?
            control-device
            control-input-event
            control-alsa-ctl
            connect-control
            define-control
            make-input-max-parameter
            configure-device
            midi-note-button
            make-midi-note-button-toggle
            remap-button
            make-remap-button-toggle
            system-button
            make-system-button-toggle
            rel-knob-to-button
            make-abs-knob-to-button
            system-abs-knob
            system-rel-knob
            make-abs-to-rel-system-knob
            midi-control-abs-knob
            make-finite-midi-control-abs-knob
            make-rel-to-abs-midi-control-knob
            midi-note-pad))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Defining "control" structures and mapping them to callback ;;
;; functions                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-record-type <control>
  (make-control input-event alsa-ctl)
  control?
  (input-event control-input-event)
  (alsa-ctl control-alsa-ctl))

(define-syntax-rule (define-control symbol input-event alsa-ctl)
  "Bind SYMBOL to the functionally-related pair INPUT-EVENT and
ALSA-CTL."
  (define symbol (make-control input-event alsa-ctl)))

(define (make-input-max-parameter max-value)
  "Parameterize MAX-VALUE, ensuring that any new values are greater
than zero but less than or equal to the original value."
  (make-parameter max-value
                  (lambda (val)
                    (if (not (integer? val))
                        (error "must be an integer"))
                    (if (or (< val 0)
                            (> val max-value))
                        (error (simple-format #f "must be a positive integer <= ~a"
                                              max-value))
                        val))))

(define* (connect-control device control callback #:key (dummy 0) #:allow-other-keys
                          #:rest r)
  "Connect CONTROL to a CALLBACK function to be called when CONTROL is
activated (i.e. when the input event associated with CONTROL occurs)."
  (let ((event (control-input-event control))
        (ctl (control-alsa-ctl control)))
    (when ctl
      (unless (alsa-ctl-descr ctl)
        (open-alsa-ctl ctl device)))
    (add-event-hook! device event
                     (lambda (value)
                       (callback device event value ctl r)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Device configuration ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
(define* (configure-device #:key alsa-name input-name
                           (open-midi-seq #t)
                           (create-input-remapper #t)
                           remap-events controls
                           (idle-wait 1.0) idle-hooks
                           exit-hooks init)
  "Open a device using ALSA device ALSA-NAME and input device
INPUT-NAME.  Set up the device further through optional arguments:

  #:open-midi-seq -- Open a MIDI sequencer (default: #t)
  #:create-input-remapper -- Create an input remapper (default: #t)
  #:remap-events -- A list of input events constructed using 'make-input-event',
                    enabling the device to remap input events to these events.
  #:controls -- A list of control definitions, each specified as a list
                (CONTROL CALLBACK OPTION VALUE ...) where each OPTION and VALUE
                specify optional keyword arguments to CALLBACK
  #:idle-wait -- The idle timer duration
  #:idle-hooks -- A list of procedures to run on the idle timer
  #:exit-hooks -- A list of procedures to run when the program exits
  #:init -- Extra device initialization routines"
  (let ((device (open-device alsa-name input-name open-midi-seq
                             create-input-remapper)))
    (when device
      (if remap-events
          (begin
            (for-each (lambda (event)
                        (remap-enable-event device
                                            (car event) (cdr event)))
                      remap-events)
            (finalize-remap-dev device)))
      (if controls
          (for-each (lambda (control-def)
                      (apply (lambda* (control callback #:rest r)
                               (apply connect-control
                                      device
                                      control
                                      callback
                                      r))
                             control-def))
                    controls))
      (if idle-wait
          (set-idle-wait! device idle-wait))
      (if idle-hooks
          (for-each (lambda (hook)
                      (add-idle-hook! device hook))
                    idle-hooks))
      (if exit-hooks
          (for-each (lambda (hook)
                      (add-exit-hook! device hook))
                    (cons (lambda () (reset-idle-hook! device)) exit-hooks)))
      (if init
          (for-each (lambda (init-proc)
                      (apply init-proc '()))
                    init)))))

;;;;;;;;;;;;;;;;;;;;;;
;; Helper functions ;;
;;;;;;;;;;;;;;;;;;;;;;
(define (scale-value input-value input-max target-max)
  (cond ((= input-max target-max) input-value)
        ((> input-max target-max)
         (floor (* (/ input-value input-max) target-max)))
        ((< input-max target-max)
         (floor (* (/ input-value target-max) input-max)))))

;;;;;;;;;;;;;;;;;;;;;;;;
;; Callback functions ;;
;;;;;;;;;;;;;;;;;;;;;;;;
(define (midi-note-button device input-event input-value alsa-ctl options)
  "Send a MIDI event to DEVICE's MIDI sequencer: if INPUT-VALUE is non-zero,
send a MIDI note-on event, otherwise send a note-off event.  If
ALSA-CTL is not #f and its descriptor points to an open ctl, toggle
the ctl (i.e. turn the LED on or off).

Optional keyword arguments:
  #:channel -- MIDI channel on which to send the event (default: 0)
  #:note -- MIDI note to send (default: note-c4)
  #:velocity -- velocity of the note (default: 127)"
  (let-keywords options #f ((channel 0)
                            (note 'note-c4)
                            (velocity 127))
    (if (> input-value 0)
        (begin
          (send-midi-noteon device channel (midi:note->number note) velocity)
          (set-ctl alsa-ctl #t))
        (begin
          (send-midi-noteoff device channel (midi:note->number note) velocity)
          (set-ctl alsa-ctl #f)))))

(define* (make-midi-note-button-toggle #:optional (init-state #f))
  "Returns a callback function to send a MIDI note-on event to
DEVICE's MIDI sequencer when toggleed on or to send a note-off event
otherwise.  If ALSA-CTL is not #f and its descriptor points to an open
ctl, toggle the ctl (i.e. turn the LED on or off).

Optional keyword arguments:
  #:channel -- MIDI channel on which to send the event (default: 0)
  #:note -- MIDI note to send (default: midi:note-c4)
  #:velocity -- velocity of the note (default: 127)"
  (let ((state init-state))
    (lambda (device input-event input-value alsa-ctl options)
      (let-keywords options #f ((channel 0)
                               (note 'note-c4)
                               (velocity 127))
        (begin
          (when (> input-value 0)
            (if (not state)
                (begin
                  (send-midi-noteon device channel (midi:note->number note)
                                    velocity)
                  (set! state #t))
                (begin
                  (send-midi-noteoff device channel (midi:note->number note)
                                     velocity)
                  (set! state #f)))
            (toggle-ctl alsa-ctl)))))))

(define (remap-button device input-event input-value alsa-ctl options)
  "Send an input event to DEVICE's input remapper.  If ALSA-CTL is not
#f and its descriptor points to an open ctl, toggle the ctl (i.e. turn
the LED on or off).

Optional keyword arguments:
  #:event-code -- Event code (default: #f)
  #:mod-keys -- List of modifier keys to apply (e.g. key-alt, key-leftctrl,
                etc.)
  #:on-value -- Event value for when the button is pressed (default: 1)
  #:off-value -- Event value for when the button is released (default: 0)"
  (let-keywords options #f ((event-code #f)
                            (mod-keys #f)
                            (on-value 1)
                            (off-value 0))
    (if (> input-value 0)
        (begin
          (when mod-keys
            (for-each (lambda (mod-key)
                        (send-remap-event device ev-key mod-key 1))
                      mod-keys))
          (send-remap-event device ev-key event-code on-value)
          (set-ctl alsa-ctl #t))
        (begin
          (send-remap-event device ev-key event-code off-value)
          (when mod-keys
            (for-each (lambda (mod-key)
                        (send-remap-event device ev-key mod-key 0))
                      mod-keys))
          (set-ctl alsa-ctl #f)))))

(define* (make-remap-button-toggle #:optional (init-state #f))
  "Returns a callback function to send an ON input event to DEVICE's
input remapper when toggleed on, and send an OFF input event when
toggleed off.  If ALSA-CTL is not #f and its descriptor points to an
open ctl, toggle on/off the ctl.

Optional keyword arguments:
  #:event-code -- Event code (default: #f)
  #:mod-keys -- List of modifier keys to apply (e.g. key-alt, key-leftctrl,
                etc.)
  #:on-value -- Event value for when the toggle is turned on (default: 1)
  #:off-value -- Event value for when the toggle is turned off (default: 0)
  #:hold-down -- \"Hold down\" the button when toggled on (default: #f)"
  (let ((state init-state))
    (lambda (device input-event input-value alsa-ctl options)
      (let-keywords options #f ((event-code #f)
                                (mod-keys #f)
                                (on-value 1)
                                (off-value 0)
                                (hold-down #f))
        (begin
          (when (> input-value 0)
            (if (not state)
                (begin
                  (when mod-keys
                    (for-each (lambda (mod-key)
                                (send-remap-event device ev-key mod-key 1))
                              mod-keys))
                  (send-remap-event device ev-key event-code on-value)
                  (unless hold-down
                    (send-remap-event device ev-key event-code off-value))
                  (when mod-keys
                    (for-each (lambda (mod-key)
                                (send-remap-event device ev-key mod-key 0))
                              mod-keys))
                  (set! state #t))
                (begin
                  (when mod-keys
                    (for-each (lambda (mod-key)
                                (send-remap-event device ev-key mod-key 1))
                              mod-keys))
                  (unless hold-down
                    (send-remap-event device ev-key event-code on-value))
                  (send-remap-event device ev-key event-code off-value)
                  (when mod-keys
                    (for-each (lambda (mod-key)
                                (send-remap-event device ev-key mod-key 0))
                              mod-keys))
                  (set! state #f)))
            (toggle-ctl alsa-ctl)))))))

(define (system-button device input-event input-value alsa-ctl
                              options)
  "Execute a system command when a button is pressed.  If ALSA-CTL is
not #f and its descriptor points to an open ctl, toggle the
ctl (i.e. turn the LED on or off).

Optional keyword arguments:
  #:cmd -- Command (default: \"echo 'Hello, world!'\")"
  (let-keywords options #f ((cmd "echo 'Hello, world!'"))
    (if (> input-value 0)
        (begin
          (system cmd)
          (set-ctl alsa-ctl #t))
        (begin
          (set-ctl alsa-ctl #f)))))

(define* (make-system-button-toggle #:optional (init-state #f))
  "Return a callback function to execute a system command when the
toggle is turned on.  If ALSA-CTL is not #f and its descriptor points
to an open ctl, turn it on/off.

Optional keyword arguments:
  #:on-cmd -- Command (default: \"echo 'Hello, world!'\")
  #:off-cmd -- Command (default: \"echo 'Goodbye, world!'\")"
  (let ((state init-state))
    (lambda (device input-event input-value alsa-ctl options)
      (let-keywords options #f ((on-cmd "echo 'Hello, world!'")
                                (off-cmd "echo 'Goodbye, world!'"))
        (begin
          (when (> input-value 0)
            (if (not state)
                (begin
                  (system on-cmd)
                  (set! state #t))
                (begin
                  (system off-cmd)
                  (set! state #f)))
            (toggle-ctl alsa-ctl)))))))

(define (rel-knob-to-button device input-event input-value alsa-ctl options)
  "Remap a relative-position event to a button/key event.  Different
events can be sent for negative and positive values.  If ALSA-CTL is
not #f and its descriptor points to an open ctl, turn on the
ctl (i.e. turn the LED on).

Optional keyword arguments:
  #:neg-event-code -- Event code for negative values (default: #f)
  #:pos-event-code -- Event code for positive values (default: #f)
  #:neg-mod-keys -- List of modifier keys to apply for negative values
                    (e.g. key-alt, key-leftctrl, etc.)
  #:pos-mod-keys -- List of modifier keys to apply for negative values
                    (e.g. key-alt, key-leftctrl, etc.)
  #:repeat -- Send multiple key events based on the magnitude of
              INPUT-VALUE (default: #f"
  (let-keywords options #f ((neg-event-code #f)
                            (pos-event-code #f)
                            (neg-mod-keys #f)
                            (pos-mod-keys #f)
                            (repeat #f))
    (let ((mod-keys (if (< input-value 0)
                        neg-mod-keys pos-mod-keys))
          (event-code (if (< input-value 0)
                          neg-event-code pos-event-code)))
      (begin
        (set-ctl alsa-ctl #t)
        (when mod-keys
          (for-each (lambda (mod-key)
                      (send-remap-event device ev-key mod-key 1))
                    mod-keys))
        (if repeat
            (do ((i 1 (1+ i)))
                ((> i (abs input-value)))
              (begin
                (send-remap-event device ev-key event-code 1)
                (send-remap-event device ev-key event-code 0)))
            (begin
              (send-remap-event device ev-key event-code 1)
              (send-remap-event device ev-key event-code 0)))
        (when mod-keys
          (for-each (lambda (mod-key)
                      (send-remap-event device ev-key mod-key 0))
                    mod-keys))
        (set-ctl alsa-ctl #f)))))

(define (make-abs-knob-to-button)
  "Return a callback function that remaps an absolute-position event
to a button event.  The absolute position is first converted to a
relative position.  Different events can be sent for negative and
positive values.  If ALSA-CTL is not #f and its descriptor points to
an open ctl, turn on the ctl (i.e. turn the LED on).

Optional keyword arguments:
  #:neg-event-code -- Event code for negative values (default: #f)
  #:pos-event-code -- Event code for positive values (default: #f)
  #:neg-mod-keys -- List of modifier keys to apply for negative values
                    (e.g. key-alt, key-leftctrl, etc.)
  #:pos-mod-keys -- List of modifier keys to apply for negative values
                    (e.g. key-alt, key-leftctrl, etc.)
  #:repeat -- Send multiple key events based on the magnitude of
              INPUT-VALUE (default: #f)
  #:knob-max -- Knob maximum value (default: 127)
  #:wrap-tol -- Relative-position tolerance before deciding that the absolute
                position has wrapped on an \"infinite-rotary\"-style knob (i.e.
                the physical knob has no start or end position and can be
                rotated completely in either direction).  Expressed as a
                fraction of KNOB-MAX (default: 0.01)
  #:invert -- Whether to invert the sign of the relative position (set if
              the sign is not what you expect for the direction of rotation)
              (default: #f)"
  (let ((prev-value #f))
    (lambda (device input-event input-value alsa-ctl options)
      (let-keywords options #f ((neg-event-code #f)
                                (pos-event-code #f)
                                (neg-mod-keys #f)
                                (pos-mod-keys #f)
                                (repeat #f)
                                (knob-max 127)
                                (wrap-tol 0.01)
                                (invert #f))
        (let* ((value (if (not prev-value)
                          0
                          (if invert
                              (- prev-value input-value)
                              (- input-value prev-value))))
               (mod-keys (if (< value 0)
                             neg-mod-keys pos-mod-keys))
               (event-code (if (< value 0)
                               neg-event-code pos-event-code)))
          (begin
            (set-ctl alsa-ctl #t)
            (when mod-keys
              (for-each (lambda (mod-key)
                          (send-remap-event device ev-key mod-key 1))
                        mod-keys))
            (if repeat
                (do ((i 1 (1+ i)))
                    ((> i (abs value)))
                  (begin
                    (send-remap-event device ev-key event-code 1)
                    (send-remap-event device ev-key event-code 0)))
                (begin
                  (send-remap-event device ev-key event-code 1)
                  (send-remap-event device ev-key event-code 0)))
            (when mod-keys
              (for-each (lambda (mod-key)
                          (send-remap-event device ev-key mod-key 0))
                        mod-keys))
            (set! prev-value input-value)
            (set-ctl alsa-ctl #f)))))))

  (define (system-abs-knob device input-event input-value alsa-ctl options)
    "Send a system event when a knob is changed.  The input value is
added to the system command via standard text formatting.  If ALSA-CTL
is not #f and its descriptor points to an open ctl, toggle the
ctl (i.e. turn the LED on or off).  This procedure applies to knobs
(or other continuous inputs) that generate absolute position values (i.e. they
give values between a fixed minimum and maximum value).

Optional keyword arguments:
  #:cmdproc -- A procedure that takes one argument (the knob value) and
               produces a string command (default:
                 (lambda (x) (simple-format #f \"printf '%d\n' ~A\" x)))
  #:max-value -- Maximum value to send to the command (default: 127)
  #:knob-max -- Knob maximum value (default: 127)"
    (let-keywords options #f ((cmdproc (lambda (x) (simple-format #f "printf '%d\\n' ~A" x)))
                              (max-value 127)
                              (knob-max 127))
                  (when (eq? (car input-event) ev-abs)
                    (let* ((value (scale-value input-value knob-max max-value)))
                      (system (cmdproc value)))
                    (set-ctl alsa-ctl #t))))

(define (system-rel-knob device input-event input-value alsa-ctl options)
  "Send a system event when a knob is changed.  The input value is
added to the system command via standard text formatting.  If ALSA-CTL
is not #f and its descriptor points to an open ctl, toggle the
ctl (i.e. turn the LED on or off).  This procedure applies to knobs
(or other continuous inputs) that generate relative position values (i.e. they
give values relative to a previous position).

Optional keyword arguments:
  #:cmdproc -- A procedure that takes one argument (the knob value) and
               produces a string command (default:
                 (lambda (x) (simple-format #f \"printf '%d\n' ~A\" x)))"
  (let-keywords options #f ((cmdproc (lambda (x) (simple-format #f "printf '%d\\n' ~A" x))))
    (when (eq? (car input-event) ev-rel)
      (system (cmdproc input-value))
      (set-ctl alsa-ctl #t))))

(define (make-abs-to-rel-system-knob)
  "Return a callback function that sends a system event when a knob is
changed.  The input value is converted from an absolute value to a
relative value and added to the system command via standard text
formatting.  If ALSA-CTL is not #f and its descriptor points to an
open ctl, toggle the ctl (i.e. turn the LED on or off).

Optional keyword arguments:
  #:cmdproc -- A procedure that takes one argument (the knob value) and
               produces a string command (default:
                 (lambda (x) (simple-format #f \"printf '%d\n' ~A\" x)))
  #:max-value -- Maximum value to send to the command (default: 127)
  #:knob-max -- Knob maximum value (default: 127)
  #:wrap-tol -- Relative-position tolerance before deciding that the absolute
                position has wrapped on an \"infinite-rotary\"-style knob (i.e.
                the physical knob has no start or end position and can be
                rotated completely in either direction).  Expressed as a
                fraction of KNOB-MAX (default: 0.01)
  #:invert -- Whether to invert the sign of the relative position (set if
              the sign is not what you expect for the direction of rotation)
              (default: #f)"
  (let ((prev-value #f))
    (lambda (device input-event input-value alsa-ctl options)
      (let-keywords* options #f ((cmdproc (lambda (x) (simple-format #f "printf '%d\\n' ~A" x)))
                                 (max-value 127)
                                 (knob-max 127)
                                 (wrap-tol 0.01)
                                 (invert #f))
        (when (eq? (car input-event) ev-abs)
          (let* ((value (if (not prev-value)
                            0
                            (if invert
                                (- prev-value input-value)
                                (- input-value prev-value)))))
            (system (cmdproc
                     (if (> (abs value)
                            (* wrap-tol knob-max))
                         (if invert
                             (- (inexact->exact
                                 (round (* wrap-tol value))))
                             (inexact->exact
                              (round (* wrap-tol value))))
                         value)))
            (set! prev-value input-value)
            (set-ctl alsa-ctl #t)))))))

(define (midi-control-abs-knob device input-event input-value alsa-ctl
                                  options)
  "Send a MIDI control event when a knob is changed.
If ALSA-CTL is not #f and its descriptor points to an open ctl, toggle
the ctl (i.e. turn the LED on or off).  This procedure applies to knobs
(or other continuous inputs) that generate absolute position values (i.e. they
give values between a fixed minimum and maximum value).

Optional keyword arguments:
  #:channel -- MIDI channel (default: 0)
  #:control -- MIDI control (default: 'mod-wheel)
  #:knob-max -- knob maximum value (default: 127)"
  (let-keywords options #f ((channel 0)
                            (control 'mod-wheel)
                            (knob-max 127))
    (when (eq? (car input-event) ev-abs)
      (let* ((value (scale-value input-value knob-max 127)))
        (send-midi-control device channel (midi:control->number control) value))
      (set-ctl alsa-ctl #t))))

(define* (make-finite-midi-control-abs-knob)
  "Return a callback function that sends a MIDI control event when a
knob is changed.  If ALSA-CTL is not #f and its descriptor points to
an open ctl, toggle the ctl (i.e. turn the LED on or off).  This
procedure applies to \"infinite rotary\" knobs that generate absolute
position values that \"wrap around\" when they reach the
minimum/maximum value.

Optional keyword arguments:
  #:channel -- MIDI channel (default: 0)
  #:control -- MIDI control (default: 'mod-wheel)
  #:knob-max -- knob maximum value (default: 127)
  #:wrap-tol -- Relative-position tolerance before deciding that the absolute
                position has wrapped on an \"infinite-rotary\"-style knob (i.e.
                the physical knob has no start or end position and can be
                rotated completely in either direction).  Expressed as a
                fraction of KNOB-MAX (default: 0.1)"
  (let ((prev-raw-value #f)
        (prev-clipped-value #f))
    (lambda (device input-event input-value alsa-ctl options)
      (let-keywords options #f ((channel 0)
                                (control 'mod-wheel)
                                (knob-max 127)
                                (wrap-tol 0.1))
        (when (eq? (car input-event) ev-abs)
          (if (not prev-raw-value)
              (begin
                (send-midi-control device channel
                                   (midi:control->number control)
                                   (scale-value input-value knob-max 127))
                (set! prev-raw-value input-value)
                (set! prev-clipped-value input-value)
                (set-ctl alsa-ctl #t))
              (let* ((raw-rel-value (- input-value prev-raw-value))
                     (rel-value (cond ((< (abs raw-rel-value) (* wrap-tol knob-max))
                                       raw-rel-value)
                                      ((> raw-rel-value (* wrap-tol knob-max))
                                       (- (+ prev-raw-value (- knob-max input-value))))
                                      (#t
                                       (+ input-value (- knob-max prev-raw-value)))))
                     (new-value (+ prev-clipped-value rel-value))
                     (clipped-value (cond ((< new-value 0) 0)
                                          ((> new-value knob-max) knob-max)
                                          (#t new-value)))
                     (value (scale-value clipped-value knob-max 127)))
                (send-midi-control device channel (midi:control->number control)
                                   value)
                (set! prev-raw-value input-value)
                (set! prev-clipped-value clipped-value)
                (set-ctl alsa-ctl #t))))))))

(define* (make-rel-to-abs-midi-control-knob #:optional (init-value 0))
  "Return a callback function that sends a MIDI control event when a
knob is changed.  If ALSA-CTL is not #f and its descriptor points to
an open ctl, toggle the ctl (i.e. turn the LED on or off).  This
procedure applies to knobs (or other continuous inputs) that generate
relative position values (i.e. they give values relative to a previous
position).  The relative values are mapped to absolute values to be
used by the MIDI control

Optional keyword arguments:
  #:channel -- MIDI channel (default: 0)
  #:control -- MIDI control (default: 'mod-wheel)"
  (let ((prev-value init-value))
    (lambda (device input-event input-value alsa-ctl options)
      (let-keywords options #f ((channel 0)
                                (control 'mod-wheel))
        (when (eq? (car input-event) ev-rel)
          (let* ((new-value (+ prev-value input-value))
                 (value (cond ((< new-value 0) 0)
                              ((> new-value 127) 127)
                              (#t new-value))))
            (send-midi-control device channel (midi:control->number control) value)
            (set! prev-value value)
            (set-ctl alsa-ctl #t)))))))

(define (midi-note-pad device input-event input-value alsa-ctl
                       options)
  "Send a MIDI note when a pad or other velocity-sensitive control is triggered.
If ALSA-CTL is not #f and its descriptor points to an open ctl, set
the ctl appropriately.

Optional keyword arguments:
  #:channel -- MIDI channel on which to send the event (default: 0)
  #:note -- MIDI note to send (default: midi:note-c4)
  #:pad-max -- pad event maximum value (default: 127)"
  (let-keywords options #f ((channel 0)
                            (note 'note-c4)
                            (pad-max 127))
    (let* ((velocity (scale-value input-value pad-max 127))
           (ctl-value
            (if (eq? (alsa-ctl-type alsa-ctl) 'integer)
                (scale-value input-value pad-max
                             (integer-ctl-max (alsa-ctl-descr alsa-ctl)))
                (not (= 0 input-value)))))
      (if (> input-value 0)
          (begin
            (send-midi-noteon device channel (midi:note->number note) velocity)
            (set-ctl alsa-ctl ctl-value))
          (begin
            (send-midi-noteoff device channel (midi:note->number note) 0)
            (set-ctl alsa-ctl ctl-value))))))
