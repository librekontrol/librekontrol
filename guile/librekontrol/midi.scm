;; Copyright (C) 2019, 2021, 2023, 2025  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol midi)
  #:use-module (rnrs enums)
  #:export (notes
            note->number
            controls
            control->number))

(define notes-str '("c" "c#" "d" "d#" "e" "f" "f#" "g" "g#" "a" "a#" "b"))

(define construct-note-name
  (lambda (i)
    (let ((note (list-ref notes-str (euclidean-remainder i 12)))
          (octave (- (floor (/ i 12)) 1)))
      (string->symbol (string-append "note-" note
                                     (number->string octave))))))

(define construct-note-names
  (lambda (i)
    (if (eq? i 128)
        '()
        (cons (construct-note-name i)
              (construct-note-names (+ i 1))))))

(define notes (make-enumeration (construct-note-names 0)))
(define note->number (enum-set-indexer notes))

(define controls '(bank-select
                   mod-wheel
                   breath-controller
                   control-3
                   foot-controller
                   portamento-time
                   data-entry-msb
                   channel-volume
                   balance
                   control-9
                   pan
                   expression-controller
                   effect-control-1
                   effect-control-2
                   control-14
                   control-15
                   general-1
                   general-2
                   general-3
                   general-4
                   control-20
                   control-21
                   control-22
                   control-23
                   control-24
                   control-25
                   control-26
                   control-27
                   control-28
                   control-29
                   control-30
                   control-31
                   lsb-control-0
                   lsb-control-1
                   lsb-control-2
                   lsb-control-3
                   lsb-control-4
                   lsb-control-5
                   lsb-control-6
                   lsb-control-7
                   lsb-control-8
                   lsb-control-9
                   lsb-control-10
                   lsb-control-11
                   lsb-control-12
                   lsb-control-13
                   lsb-control-14
                   lsb-control-15
                   lsb-control-16
                   lsb-control-17
                   lsb-control-18
                   lsb-control-19
                   lsb-control-20
                   lsb-control-21
                   lsb-control-22
                   lsb-control-23
                   lsb-control-24
                   lsb-control-25
                   lsb-control-26
                   lsb-control-27
                   lsb-control-28
                   lsb-control-29
                   lsb-control-30
                   lsb-control-31
                   damper-pedal
                   portamento
                   sostenuto
                   soft-pedal
                   legato-footswitch
                   hold-2
                   sound-controller-1
                   sound-controller-2
                   sound-controller-3
                   sound-controller-4
                   sound-controller-5
                   sound-controller-6
                   sound-controller-7
                   sound-controller-8
                   sound-controller-9
                   sound-controller-10
                   general-5
                   general-6
                   general-7
                   general-8
                   portamento-control
                   control-85
                   control-86
                   control-87
                   high-res-velocity
                   control-89
                   control-90
                   effects-1-depth
                   effects-2-depth
                   effects-3-depth
                   effects-4-depth
                   effects-5-depth
                   data-incr
                   data-decr
                   nrpn-lsb
                   nrpn-msb
                   rpn-lsb
                   rpn-msb
                   control-102
                   control-103
                   control-104
                   control-105
                   control-106
                   control-107
                   control-108
                   control-109
                   control-110
                   control-111
                   control-112
                   control-113
                   control-114
                   control-115
                   control-116
                   control-117
                   control-118
                   control-119
                   all-sound-off
                   reset-all-controllers
                   local-control
                   all-notes-off
                   omni-mode-off
                   omni-mode-on
                   mono-mode-on
                   poly-mode-on))
(define controls (make-enumeration controls))
(define control->number (enum-set-indexer controls))
