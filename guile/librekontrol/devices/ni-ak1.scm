;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-ak1)
  #:use-module (librekontrol input)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol device)
  #:export (alsa-name
            input-name
            button-left
            button-middle
            button-right
            knob-ring
            led-left
            led-middle
            led-right
            led-ring
            left
            middle
            right
            knob
            knob-max))

(define alsa-name "AudioKontrol1")
(define input-name "Audio Kontrol 1")

(define-input-event button-left ev-key key-a)
(define-input-event button-middle ev-key key-b)
(define-input-event button-right ev-key key-c)
(define-input-event knob-ring ev-abs abs-x)

(define-alsa-ctl led-left 1 'boolean)
(define-alsa-ctl led-middle 2 'boolean)
(define-alsa-ctl led-right 3 'boolean)
(define-alsa-ctl led-ring 4 'boolean)

(define-control left button-left led-left)
(define-control middle button-middle led-middle)
(define-control right button-right led-right)
(define-control knob knob-ring led-ring)

(define knob-max (make-input-max-parameter 999))
