;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-maschine)
  #:use-module (librekontrol input)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol device)
  #:export (alsa-name
            input-name
            led-pad-1
            led-pad-2
            led-pad-3
            led-pad-4
            led-pad-5
            led-pad-6
            led-pad-7
            led-pad-8
            led-pad-9
            led-pad-10
            led-pad-11
            led-pad-12
            led-pad-13
            led-pad-14
            led-pad-15
            led-pad-16
            button-pad-1
            button-pad-2
            button-pad-3
            button-pad-4
            button-pad-5
            button-pad-6
            button-pad-7
            button-pad-8
            button-pad-9
            button-pad-10
            button-pad-11
            button-pad-12
            button-pad-13
            button-pad-14
            button-pad-15
            button-pad-16
            pad-1
            pad-2
            pad-3
            pad-4
            pad-5
            pad-6
            pad-7
            pad-8
            pad-9
            pad-10
            pad-11
            pad-12
            pad-13
            pad-14
            pad-15
            pad-16
            led-mute
            led-solo
            led-select
            led-duplicate
            led-navigate
            led-pad-mode
            led-pattern
            led-scene
            button-mute
            button-solo
            button-select
            button-duplicate
            button-navigate
            button-pad-mode
            button-pattern
            button-scene
            mute
            solo
            select
            duplicate
            navigate
            pad-mode
            pattern
            scene
            led-shift
            led-erase
            led-grid
            led-right-bottom
            led-rec
            led-play
            led-left-bottom
            led-restart
            button-shift
            button-erase
            button-grid
            button-right-bottom
            button-rec
            button-play
            button-left-bottom
            button-restart
            shift
            erase
            grid
            right-bottom
            led-right-bottom
            rec
            play
            left-bottom
            led-left-bottom
            restart
            led-group-a
            led-group-b
            led-group-c
            led-group-d
            led-group-e
            led-group-f
            led-group-g
            led-group-h
            button-group-a
            button-group-b
            button-group-c
            button-group-d
            button-group-e
            button-group-f
            button-group-g
            button-group-h
            group-a
            group-b
            group-c
            group-d
            group-e
            group-f
            group-g
            group-h
            led-auto-write
            led-snap
            led-right-top
            led-left-top
            led-sampling
            led-browse
            led-step
            led-control
            button-auto-write
            button-snap
            button-right-top
            button-left-top
            button-sampling
            button-browse
            button-step
            button-control
            auto-write
            led-auto-write
            snap
            right-top
            led-right-top
            left-top
            led-left-top
            sampling
            browse
            step
            control
            led-top-1
            led-top-2
            led-top-3
            led-top-4
            led-top-5
            led-top-6
            led-top-7
            led-top-8
            button-top-1
            button-top-2
            button-top-3
            button-top-4
            button-top-5
            button-top-6
            button-top-7
            button-top-8
            top-1
            top-2
            top-3
            top-4
            top-5
            top-6
            top-7
            top-8
            knob-volume
            knob-tempo
            knob-swing
            button-note-repeat
            volume
            tempo
            swing
            note-repeat
            backlight-display
            knob-1-in
            knob-2-in
            knob-3-in
            knob-4-in
            knob-5-in
            knob-6-in
            knob-7-in
            knob-8-in
            knob-1
            knob-2
            knob-3
            knob-4
            knob-5
            knob-6
            knob-7
            knob-8
            pad-max
            knob-max))

(define alsa-name "MaschineControl")
(define input-name "Maschine Controller")

(define-syntax-rule (btn x)
  (+ x btn-misc))

(define-syntax-rule (pad x)
  (+ x abs-pressure))

;; Pads
(define-alsa-ctl led-pad-1 1 'integer)
(define-alsa-ctl led-pad-2 2 'integer)
(define-alsa-ctl led-pad-3 3 'integer)
(define-alsa-ctl led-pad-4 4 'integer)
(define-alsa-ctl led-pad-5 5 'integer)
(define-alsa-ctl led-pad-6 6 'integer)
(define-alsa-ctl led-pad-7 7 'integer)
(define-alsa-ctl led-pad-8 8 'integer)
(define-alsa-ctl led-pad-9 9 'integer)
(define-alsa-ctl led-pad-10 10 'integer)
(define-alsa-ctl led-pad-11 11 'integer)
(define-alsa-ctl led-pad-12 12 'integer)
(define-alsa-ctl led-pad-13 13 'integer)
(define-alsa-ctl led-pad-14 14 'integer)
(define-alsa-ctl led-pad-15 15 'integer)
(define-alsa-ctl led-pad-16 16 'integer)

(define-input-event button-pad-1 ev-abs (pad 12))
(define-input-event button-pad-2 ev-abs (pad 13))
(define-input-event button-pad-3 ev-abs (pad 14))
(define-input-event button-pad-4 ev-abs (pad 15))
(define-input-event button-pad-5 ev-abs (pad 8))
(define-input-event button-pad-6 ev-abs (pad 9))
(define-input-event button-pad-7 ev-abs (pad 10))
(define-input-event button-pad-8 ev-abs (pad 11))
(define-input-event button-pad-9 ev-abs (pad 4))
(define-input-event button-pad-10 ev-abs (pad 5))
(define-input-event button-pad-11 ev-abs (pad 6))
(define-input-event button-pad-12 ev-abs (pad 7))
(define-input-event button-pad-13 ev-abs (pad 0))
(define-input-event button-pad-14 ev-abs (pad 1))
(define-input-event button-pad-15 ev-abs (pad 2))
(define-input-event button-pad-16 ev-abs (pad 3))

(define-control pad-1 button-pad-1 led-pad-1)
(define-control pad-2 button-pad-2 led-pad-2)
(define-control pad-3 button-pad-3 led-pad-3)
(define-control pad-4 button-pad-4 led-pad-4)
(define-control pad-5 button-pad-5 led-pad-5)
(define-control pad-6 button-pad-6 led-pad-6)
(define-control pad-7 button-pad-7 led-pad-7)
(define-control pad-8 button-pad-8 led-pad-8)
(define-control pad-9 button-pad-9 led-pad-9)
(define-control pad-10 button-pad-10 led-pad-10)
(define-control pad-11 button-pad-11 led-pad-11)
(define-control pad-12 button-pad-12 led-pad-12)
(define-control pad-13 button-pad-13 led-pad-13)
(define-control pad-14 button-pad-14 led-pad-14)
(define-control pad-15 button-pad-15 led-pad-15)
(define-control pad-16 button-pad-16 led-pad-16)

;; Middle
(define-alsa-ctl led-mute 17 'integer)
(define-alsa-ctl led-solo 18 'integer)
(define-alsa-ctl led-select 19 'integer)
(define-alsa-ctl led-duplicate 20 'integer)
(define-alsa-ctl led-navigate 21 'integer)
(define-alsa-ctl led-pad-mode 22 'integer)
(define-alsa-ctl led-pattern 23 'integer)
(define-alsa-ctl led-scene 24 'integer)

(define-input-event button-mute ev-key (btn 40))
(define-input-event button-solo ev-key (btn 39))
(define-input-event button-select ev-key (btn 38))
(define-input-event button-duplicate ev-key (btn 37))
(define-input-event button-navigate ev-key (btn 36))
(define-input-event button-pad-mode ev-key (btn 35))
(define-input-event button-pattern ev-key (btn 34))
(define-input-event button-scene ev-key (btn 33))

(define-control mute button-mute led-mute)
(define-control solo button-solo led-solo)
(define-control select button-select led-select)
(define-control duplicate button-duplicate led-duplicate)
(define-control navigate button-navigate led-navigate)
(define-control pad-mode button-pad-mode led-pad-mode)
(define-control pattern button-pattern led-pattern)
(define-control scene button-scene led-scene)

;; Transport
(define-alsa-ctl led-shift 25 'integer)
(define-alsa-ctl led-erase 26 'integer)
(define-alsa-ctl led-grid 27 'integer)
(define-alsa-ctl led-right-bottom 28 'integer)
(define-alsa-ctl led-rec 29 'integer)
(define-alsa-ctl led-play 30 'integer)
(define-alsa-ctl led-left-bottom 31 'integer)
(define-alsa-ctl led-restart 32 'integer)

(define-input-event button-shift ev-key (btn 32))
(define-input-event button-erase ev-key (btn 31))
(define-input-event button-grid ev-key (btn 28))
(define-input-event button-right-bottom ev-key (btn 27))
(define-input-event button-rec ev-key (btn 30))
(define-input-event button-play ev-key (btn 29))
(define-input-event button-left-bottom ev-key (btn 26))
(define-input-event button-restart ev-key (btn 25))

(define-control shift button-shift led-shift)
(define-control erase button-erase led-erase)
(define-control grid button-grid led-grid)
(define-control right-bottom button-right-bottom led-right-bottom)
(define-control rec button-rec led-rec)
(define-control play button-play led-play)
(define-control left-bottom button-left-bottom led-left-bottom)
(define-control restart button-restart led-restart)

; Groups
(define-alsa-ctl led-group-a 33 'integer)
(define-alsa-ctl led-group-b 34 'integer)
(define-alsa-ctl led-group-c 35 'integer)
(define-alsa-ctl led-group-d 36 'integer)
(define-alsa-ctl led-group-e 37 'integer)
(define-alsa-ctl led-group-f 38 'integer)
(define-alsa-ctl led-group-g 39 'integer)
(define-alsa-ctl led-group-h 40 'integer)

(define-input-event button-group-a ev-key (btn 17))
(define-input-event button-group-b ev-key (btn 18))
(define-input-event button-group-c ev-key (btn 19))
(define-input-event button-group-d ev-key (btn 20))
(define-input-event button-group-e ev-key (btn 24))
(define-input-event button-group-f ev-key (btn 23))
(define-input-event button-group-g ev-key (btn 22))
(define-input-event button-group-h ev-key (btn 21))

(define-control group-a button-group-a led-group-a)
(define-control group-b button-group-b led-group-b)
(define-control group-c button-group-c led-group-c)
(define-control group-d button-group-d led-group-d)
(define-control group-e button-group-e led-group-e)
(define-control group-f button-group-f led-group-f)
(define-control group-g button-group-g led-group-g)
(define-control group-h button-group-h led-group-h)

; Top-left
(define-alsa-ctl led-auto-write 41 'integer)
(define-alsa-ctl led-snap 42 'integer)
(define-alsa-ctl led-right-top 43 'integer)
(define-alsa-ctl led-left-top 44 'integer)
(define-alsa-ctl led-sampling 45 'integer)
(define-alsa-ctl led-browse 46 'integer)
(define-alsa-ctl led-step 47 'integer)
(define-alsa-ctl led-control 48 'integer)

(define-input-event button-auto-write ev-key (btn 7))
(define-input-event button-snap ev-key (btn 6))
(define-input-event button-right-top ev-key (btn 5))
(define-input-event button-left-top ev-key (btn 4))
(define-input-event button-sampling ev-key (btn 3))
(define-input-event button-browse ev-key (btn 2))
(define-input-event button-step ev-key (btn 1))
(define-input-event button-control ev-key (btn 0))

(define-control auto-write button-auto-write led-auto-write)
(define-control snap button-snap led-snap)
(define-control right-top button-right-top led-right-top)
(define-control left-top button-left-top led-left-top)
(define-control sampling button-sampling led-sampling)
(define-control browse button-browse led-browse)
(define-control step button-step led-step)
(define-control control button-control led-control)

; Top
(define-alsa-ctl led-top-1 49 'integer)
(define-alsa-ctl led-top-2 50 'integer)
(define-alsa-ctl led-top-3 51 'integer)
(define-alsa-ctl led-top-4 52 'integer)
(define-alsa-ctl led-top-5 53 'integer)
(define-alsa-ctl led-top-6 54 'integer)
(define-alsa-ctl led-top-7 55 'integer)
(define-alsa-ctl led-top-8 56 'integer)

(define-input-event button-top-1 ev-key (btn 8))
(define-input-event button-top-2 ev-key (btn 9))
(define-input-event button-top-3 ev-key (btn 10))
(define-input-event button-top-4 ev-key (btn 11))
(define-input-event button-top-5 ev-key (btn 12))
(define-input-event button-top-6 ev-key (btn 13))
(define-input-event button-top-7 ev-key (btn 14))
(define-input-event button-top-8 ev-key (btn 15))

(define-control top-1 button-top-1 led-top-1)
(define-control top-2 button-top-2 led-top-2)
(define-control top-3 button-top-3 led-top-3)
(define-control top-4 button-top-4 led-top-4)
(define-control top-5 button-top-5 led-top-5)
(define-control top-6 button-top-6 led-top-6)
(define-control top-7 button-top-7 led-top-7)
(define-control top-8 button-top-8 led-top-8)

; Master
(define-input-event knob-volume ev-abs abs-rx)
(define-input-event knob-tempo ev-abs abs-ry)
(define-input-event knob-swing ev-abs abs-rz)
(define-input-event button-note-repeat ev-key (btn 16))

(define-control volume knob-volume #f)
(define-control tempo knob-tempo #f)
(define-control swing knob-swing #f)
(define-control note-repeat button-note-repeat #f)

; Misc
(define-alsa-ctl backlight-display 57 'integer)

(define-input-event knob-1-in ev-abs abs-hat0x)
(define-input-event knob-2-in ev-abs abs-hat0y)
(define-input-event knob-3-in ev-abs abs-hat1x)
(define-input-event knob-4-in ev-abs abs-hat1y)
(define-input-event knob-5-in ev-abs abs-hat2x)
(define-input-event knob-6-in ev-abs abs-hat2y)
(define-input-event knob-7-in ev-abs abs-hat3x)
(define-input-event knob-8-in ev-abs abs-hat3y)

(define-control knob-1 knob-1-in #f)
(define-control knob-2 knob-2-in #f)
(define-control knob-3 knob-3-in #f)
(define-control knob-4 knob-4-in #f)
(define-control knob-5 knob-5-in #f)
(define-control knob-6 knob-6-in #f)
(define-control knob-7 knob-7-in #f)
(define-control knob-8 knob-8-in #f)

;; Constants
(define pad-max (make-input-max-parameter 4096))
(define knob-max (make-input-max-parameter 999))
