;; Copyright (C) 2019, 2020  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-rk3)
  #:use-module (librekontrol input)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol device)
  #:export (alsa-name
            input-name
            led-1
            led-2
            led-3
            led-4
            led-5
            led-6
            led-7
            led-8
            led-pedal
            button-1-in
            button-2-in
            button-3-in
            button-4-in
            button-5-in
            button-6-in
            button-7-in
            button-8-in
            button-pedal
            button-1
            button-2
            button-3
            button-4
            button-5
            button-6
            button-7
            button-8
            pedal
            led-7seg-1a
            led-7seg-1b
            led-7seg-1c
            led-7seg-1d
            led-7seg-1e
            led-7seg-1f
            led-7seg-1g
            led-7seg-1p
            led-7seg-2a
            led-7seg-2b
            led-7seg-2c
            led-7seg-2d
            led-7seg-2e
            led-7seg-2f
            led-7seg-2g
            led-7seg-2p
            led-7seg-3a
            led-7seg-3b
            led-7seg-3c
            led-7seg-3d
            led-7seg-3e
            led-7seg-3f
            led-7seg-3g
            led-7seg-3p
            led-7seg-4a
            led-7seg-4b
            led-7seg-4c
            led-7seg-4d
            led-7seg-4e
            led-7seg-4f
            led-7seg-4g
            led-7seg-4p
            trs-pedal-in-1
            trs-pedal-in-2
            pedal-expression
            pedal-in-1
            pedal-in-2
            expression
            trs-max))

(define alsa-name "RigKontrol3")
(define input-name "RigKontrol3")

;; Main
(define-alsa-ctl led-1 37 'boolean)
(define-alsa-ctl led-2 38 'boolean)
(define-alsa-ctl led-3 39 'boolean)
(define-alsa-ctl led-4 40 'boolean)
(define-alsa-ctl led-5 33 'boolean)
(define-alsa-ctl led-6 34 'boolean)
(define-alsa-ctl led-7 35 'boolean)
(define-alsa-ctl led-8 46 'boolean)
(define-alsa-ctl led-pedal 41 'boolean)

(define-input-event button-1-in ev-key key-5)
(define-input-event button-2-in ev-key key-6)
(define-input-event button-3-in ev-key key-7)
(define-input-event button-4-in ev-key key-8)
(define-input-event button-5-in ev-key key-1)
(define-input-event button-6-in ev-key key-2)
(define-input-event button-7-in ev-key key-3)
(define-input-event button-8-in ev-key key-4)
(define-input-event button-pedal ev-key key-9)

(define-control button-1 button-1-in led-1)
(define-control button-2 button-2-in led-2)
(define-control button-3 button-3-in led-3)
(define-control button-4 button-4-in led-4)
(define-control button-5 button-5-in led-5)
(define-control button-6 button-6-in led-6)
(define-control button-7 button-7-in led-7)
(define-control button-8 button-8-in led-8)
(define-control pedal button-pedal led-pedal)

;; 7-segment LED
(define-alsa-ctl led-7seg-1a 1 'boolean)
(define-alsa-ctl led-7seg-1b 2 'boolean)
(define-alsa-ctl led-7seg-1c 3 'boolean)
(define-alsa-ctl led-7seg-1d 4 'boolean)
(define-alsa-ctl led-7seg-1e 5 'boolean)
(define-alsa-ctl led-7seg-1f 6 'boolean)
(define-alsa-ctl led-7seg-1g 7 'boolean)
(define-alsa-ctl led-7seg-1p 8 'boolean)

(define-alsa-ctl led-7seg-2a 9 'boolean)
(define-alsa-ctl led-7seg-2b 10 'boolean)
(define-alsa-ctl led-7seg-2c 11 'boolean)
(define-alsa-ctl led-7seg-2d 12 'boolean)
(define-alsa-ctl led-7seg-2e 13 'boolean)
(define-alsa-ctl led-7seg-2f 14 'boolean)
(define-alsa-ctl led-7seg-2g 15 'boolean)
(define-alsa-ctl led-7seg-2p 16 'boolean)

(define-alsa-ctl led-7seg-3a 17 'boolean)
(define-alsa-ctl led-7seg-3b 18 'boolean)
(define-alsa-ctl led-7seg-3c 19 'boolean)
(define-alsa-ctl led-7seg-3d 20 'boolean)
(define-alsa-ctl led-7seg-3e 21 'boolean)
(define-alsa-ctl led-7seg-3f 22 'boolean)
(define-alsa-ctl led-7seg-3g 23 'boolean)
(define-alsa-ctl led-7seg-3p 24 'boolean)

(define-alsa-ctl led-7seg-4a 25 'boolean)
(define-alsa-ctl led-7seg-4b 26 'boolean)
(define-alsa-ctl led-7seg-4c 27 'boolean)
(define-alsa-ctl led-7seg-4d 28 'boolean)
(define-alsa-ctl led-7seg-4e 29 'boolean)
(define-alsa-ctl led-7seg-4f 30 'boolean)
(define-alsa-ctl led-7seg-4g 31 'boolean)
(define-alsa-ctl led-7seg-4p 32 'boolean)

;; Pedal
(define-input-event trs-pedal-in-1 ev-abs abs-x)
(define-input-event trs-pedal-in-2 ev-abs abs-y)
(define-input-event pedal-expression ev-abs abs-z)

(define-control pedal-in-1 trs-pedal-in-1 #f)
(define-control pedal-in-2 trs-pedal-in-2 #f)
(define-control expression pedal-expression #f)

;; Constants
(define trs-max (make-input-max-parameter 1024))
