 /* 
 * lk-device.c --- 
 * 
 * Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "lk-device.h"

#define _(String) gettext (String)
#define gettext_noop(String) String
#define N_(String) gettext_noop (String)

pthread_mutex_t mutex;
pthread_mutexattr_t mutexattr;

static size_t
get_device_id (lk_device_t *device, const char *device_str)
{
  if (!device)
    {
      return -1;
    }
  int i = snd_card_get_index (device_str);
  if (i >= 0 && i < 32)
    {
      return snprintf (device->id, 64, "hw:%i", i);
    }
  else
    {
      return -1;
    }
}

int
grab_alsa_ctl (lk_device_t *device, const char *controller_name)
{
  if (!device)
    {
      return -1;
    }
  device->ctl_handle = NULL;
  strncpy (device->str_id, controller_name, 63);
  device->str_id[63] = '\0';
  int err = get_device_id (device, controller_name);
  if (err < 0)
    {
      error (0, 0, _("Device not found: %s"), device->str_id);
      return err;
    }
  err = snd_ctl_open (&device->ctl_handle, device->id, 0);
  if (err < 0)
    {
      error (0, 0, _("Could not open device %s control: %s"), controller_name,
             snd_strerror (err));
      return err;
    }
  return 0;
}

int
grab_alsa_seq (lk_device_t *device, const char *controller_name)
{
  char seq_client[48] = "Librekontrol ";
  if (!device)
    {
      return -1;
    }
  device->seq_handle = NULL;
  int err = snd_seq_open (&device->seq_handle, "default", SND_SEQ_OPEN_DUPLEX, 0);
  if (err < 0)
    {
      error (0, 0, _("Could not open device %s sequencer: %s"), controller_name,
             snd_strerror (err));
      return err;
    }
  strncat (seq_client, device->str_id, 28);
  err = snd_seq_set_client_name (device->seq_handle, seq_client);
  if (err < 0)
    {
      snd_seq_close (device->seq_handle);
      error (0, 0, _("Could not set MIDI sequencer client name"));
      return err;
    }
  strcat (seq_client, " read");
  device->seq_read_port = snd_seq_create_simple_port
    (device->seq_handle, seq_client,
     (SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ),
     SND_SEQ_PORT_TYPE_APPLICATION);
  if (device->seq_read_port < 0)
    {
      return (device->seq_read_port);
    }
  size_t seq_client_len = 13;
  seq_client_len += strlen (device->str_id) > 28 ? 28 : strlen (device->str_id);
  strcpy (seq_client + seq_client_len, " write");
  seq_client[13 + strlen (device->str_id) + 7] = '\0';
  device->seq_write_port = snd_seq_create_simple_port
    (device->seq_handle, seq_client,
     (SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE),
     SND_SEQ_PORT_TYPE_APPLICATION);
  if (device->seq_write_port < 0)
    {
      return (device->seq_write_port);
    }
  return 0;
}

void
grab_input_dev (lk_device_t *device, const char *input_name)
{
  if (!device)
    {
      return;
    }
  glob_t event_glob;
  int err = glob ("/dev/input/event*", GLOB_ERR | GLOB_NOSORT, NULL, &event_glob);
  if (err)
    {
      if (err == GLOB_ABORTED)
        {
          globfree (&event_glob);
          error (EXIT_FAILURE, errno, _("Failed to glob the /dev/input "
                                        "directory"));
        }
      if (err == GLOB_NOMATCH)
        {
          globfree (&event_glob);
          error (EXIT_FAILURE, errno, _("No devices found in /dev/input"));
        }
      if (err == GLOB_NOSPACE)
        {
          error (EXIT_FAILURE, errno, _("Memory exhausted while globbing "
                                        "/dev/input"));
        }
    }
  for (size_t i=0; i < event_glob.gl_pathc; i++)
    {
      device->input_fd = open (event_glob.gl_pathv[i], O_RDONLY);
      err = libevdev_new_from_fd (device->input_fd, &(device->input_dev));
      if (err)
        continue;
      if (!strcmp (libevdev_get_name (device->input_dev), input_name))
        {
          return;
        }
      libevdev_free (device->input_dev);
      close (device->input_fd);
    }
  globfree (&event_glob);
}

void
grab_remap_dev (lk_device_t *device, const char *input_name)
{
  char *remap_name = (char *)malloc (strlen (input_name) + 9);
  if (!remap_name)
    {
      error (EXIT_FAILURE, errno, _("Memory exhausted"));
    }
  strcpy (remap_name, input_name);
  strcat (remap_name, " (remap)");
  device->remap_dev = libevdev_new ();
  libevdev_set_name (device->remap_dev, remap_name);
  free (remap_name);
}

void
lk_init ()
{
  pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE_NP);
  pthread_mutexattr_init(&mutexattr);
  pthread_mutex_init (&mutex, &mutexattr);
  pthread_mutexattr_destroy(&mutexattr);
}

void
lk_cleanup ()
{
  pthread_mutex_destroy (&mutex);
}

int
open_device (lk_device_t *device, const char *controller_name,
             const char *input_name, bool open_midi_seq,
             bool open_remap_evdev)
{
  device->ctl_handle = NULL;
  device->seq_handle = NULL;
  device->input_dev = NULL;
  device->remap_dev = NULL;
  device->uidev = NULL;
  if (!controller_name && !input_name)
    {
      error (0, 0, _("Empty device definition."));
      return -1;
    }
  if (controller_name)
    {
      int err = grab_alsa_ctl (device, controller_name);
      if (err < 0)
        {
          return err;
        }
    }
  else
    {
      strncpy (device->str_id, input_name, 63);
      device->str_id[63] = '\0';
    }
  if (input_name)
    {
      grab_input_dev (device, input_name);
      if (open_remap_evdev)
        {
          grab_remap_dev (device, input_name);
        }
    }
  if (open_midi_seq)
    {
      int err;
      if (controller_name)
        {
          err = grab_alsa_seq (device, controller_name);
        }
      else
        {
          err = grab_alsa_seq (device, input_name);
        }
      if (err < 0)
        {
          return err;
        }
    }
 return 0;
}

int
close_device (lk_device_t *device)
{
  if (device->ctl_handle)
    {
      int err = snd_ctl_close (device->ctl_handle);
      if (err)
        {
          error (0, 0, _("Failed to close the %s control handle"), device->str_id);
        }
    }
  if (device->seq_handle)
    {
      int err = snd_seq_close (device->seq_handle);
      if (err)
        {
          error (0, 0, _("Failed to close the %s sequencer handle"),
                 device->str_id);
        }
    }
  if (device->input_dev)
    {
      libevdev_free (device->input_dev);
    }
  if (device->remap_dev)
    {
      libevdev_free (device->remap_dev);
    }
  if (device->uidev)
    {
      libevdev_uinput_destroy (device->uidev);
    }
  if (close (device->input_fd))
    {
      error (0, errno, _("Failed to close the %s input file"), device->str_id);
    }
  return 0;
}

int
open_control (lk_device_ctl_t *ctl, lk_device_t *device, const char *ctl_id_str)
{
  ctl->device = device;
  ctl->elem_id = NULL;
  ctl->info = NULL;
  ctl->value = NULL;
  if (!device || !device->ctl_handle)
    {
      error (0, 0, _("Cannot open control: device does not exist"));
      return -1;
    }
  if (snd_ctl_elem_info_malloc (&ctl->info) ||
      snd_ctl_elem_id_malloc (&ctl->elem_id) ||
      snd_ctl_elem_value_malloc (&ctl->value))
    error (EXIT_FAILURE, 0, _("Memory exhausted"));
  if (snd_ctl_ascii_elem_id_parse (ctl->elem_id, ctl_id_str))
    {
      error (0, 0, _("Unable to parse control identifier: %s"), ctl_id_str);
      return -1;
    }
  snd_ctl_elem_info_set_id (ctl->info, ctl->elem_id);
  int err = snd_ctl_elem_info (device->ctl_handle, ctl->info);
  if (err < 0)
    {
      error (0, 0, _("Cannot find the given element from control %s: %s"),
             device->id, snd_strerror (err));
      return err;
    }
  snd_ctl_elem_info_get_id (ctl->info, ctl->elem_id);
  snd_ctl_elem_value_set_id (ctl->value, ctl->elem_id);
  err = snd_ctl_elem_read (device->ctl_handle, ctl->value);
  if (err < 0)
    {
      error (0, 0, _("Cannot read the element from control %s: %s"),
             device->id, snd_strerror (err));
      return err;
    }
  strncpy (ctl->id, ctl_id_str, 64);
  return 0;
}

int
close_control (lk_device_ctl_t *ctl)
{
  if (ctl->info)
    {
      snd_ctl_elem_info_free (ctl->info);
    }
  if (ctl->elem_id)
    {
      snd_ctl_elem_id_free (ctl->elem_id);
    }
  if (ctl->value)
    {
      snd_ctl_elem_value_free (ctl->value);
    }
  return 0;
}

int
check_ctl_state (const lk_device_ctl_t *ctl)
{
  if (!ctl)
    {
      error (0, 0, _("The requested control does not exist"));
      return -1;
    }
  if (!ctl->device)
    {
      error (0, 0, _("The device associated with the control %s has been lost."),
             ctl->id);
      return -1;
    }
  if (!ctl->elem_id)
    {
      error (0, 0, _("The element ID for %s on %s has been lost."),
             ctl->id, ctl->device->id);
      return -1;
    }
  if (!ctl->info)
    {
      error (0, 0, _("The control element info for %s on device %s "
                     "has been lost."), ctl->id, ctl->device->id);
      return -1;
    }
  if (!ctl->value)
    {
      error (0, 0, _("The control element value for %s on device %s "
                     "has been lost."), ctl->id, ctl->device->id);
      return -1;
    }
  if (!snd_ctl_elem_info_is_writable (ctl->info))
    {
      error (0, 0, _("Cannot write to control %s on device %s"),
             ctl->id, ctl->device->id);
      return -1;
    }
  if (!snd_ctl_elem_info_is_readable (ctl->info))
    {
      error (0, 0, _("Cannot read from control %s on device %s"),
             ctl->id, ctl->device->id);
      return -1;
    }
  return 0;
}

int
set_boolean_ctl (lk_device_ctl_t *ctl, bool value)
{
  pthread_mutex_lock (&mutex);
  int err = check_ctl_state (ctl);
  if (err)
    {
      pthread_mutex_unlock (&mutex);
      return err;
    }
  if (snd_ctl_elem_info_get_type (ctl->info) != SND_CTL_ELEM_TYPE_BOOLEAN)
    {
      error (0, 0, _("%s on device %s is not a boolean control"),
             ctl->id, ctl->device->id);
      pthread_mutex_unlock (&mutex);
      return -1;
    }
  long int old_value = snd_ctl_elem_value_get_boolean (ctl->value, 0);
  if (old_value == (long int) value)
    {
      pthread_mutex_unlock (&mutex);
      return 1;
    }
  snd_ctl_elem_value_set_boolean (ctl->value, 0, (long int) value);
  err = snd_ctl_elem_write (ctl->device->ctl_handle, ctl->value);
  if (err < 0)
    {
      error (0, 0, _("Could not write to element for control %s: %s"),
             ctl->device->id, snd_strerror (err));
      pthread_mutex_unlock (&mutex);
      return err;
    }
  pthread_mutex_unlock (&mutex);
  return 0;
}

bool
get_boolean_ctl (lk_device_ctl_t *ctl)
{
  pthread_mutex_lock (&mutex);
  int err = check_ctl_state (ctl);
  if (err)
    {
      pthread_mutex_unlock (&mutex);
      return err;
    }
  if (snd_ctl_elem_info_get_type (ctl->info) != SND_CTL_ELEM_TYPE_BOOLEAN)
    {
      error (0, 0, _("%s is not a boolean control"), ctl->id);
      pthread_mutex_unlock (&mutex);
      return -1;
    }
  bool value = snd_ctl_elem_value_get_boolean (ctl->value, 0);
  pthread_mutex_unlock (&mutex);
  return value;
}

int
toggle_boolean_ctl (lk_device_ctl_t *ctl)
{
  int err = check_ctl_state (ctl);
  if (err)
    return err;
  bool cur_value = get_boolean_ctl (ctl);
  err = set_boolean_ctl (ctl, !cur_value);
  return err;
}

int
set_integer_ctl (const lk_device_ctl_t *ctl, long int value)
{
  int err = check_ctl_state (ctl);
  if (err)
    {
        pthread_mutex_unlock (&mutex);
        return err;
    }
  if (snd_ctl_elem_info_get_type (ctl->info) != SND_CTL_ELEM_TYPE_INTEGER)
    {
      error (0, 0, _("%s on device %s is not an integer control"),
             ctl->id, ctl->device->id);
      pthread_mutex_unlock (&mutex);
      return -1;
    }
  long int min_val = snd_ctl_elem_info_get_min (ctl->info);
  long int max_val = snd_ctl_elem_info_get_max (ctl->info);
  if (value > max_val || value < min_val)
    {
      error (0, 0, _("Invalid value (%ld < x < %ld)"), min_val, max_val);
      pthread_mutex_unlock (&mutex);
      return -1;
    }
  long int old_value = snd_ctl_elem_value_get_integer (ctl->value, 0);
  if (old_value == value)
    {
      pthread_mutex_unlock (&mutex);
      return 1;
    }
  snd_ctl_elem_value_set_integer (ctl->value, 0, value);
  err = snd_ctl_elem_write (ctl->device->ctl_handle, ctl->value);
  if (err < 0)
    {
      error (0, 0, _("Could not write to element for control %s: %s"),
             ctl->device->id, snd_strerror (err));
      pthread_mutex_unlock (&mutex);
      return err;
    }
  pthread_mutex_unlock (&mutex);
  return 0;
}

long int
get_integer_ctl (const lk_device_ctl_t *ctl)
{
  int err = check_ctl_state (ctl);
  if (err)
    return err;
  if (snd_ctl_elem_info_get_type (ctl->info) != SND_CTL_ELEM_TYPE_INTEGER)
    {
      error (0, 0, _("%s is not an integer control"), ctl->id);
      return -1;
    }
  return snd_ctl_elem_value_get_integer (ctl->value, 0);
}

long int
integer_ctl_max (const lk_device_ctl_t *ctl)
{
  int err = check_ctl_state (ctl);
  if (err)
    return err;
  if (snd_ctl_elem_info_get_type (ctl->info) != SND_CTL_ELEM_TYPE_INTEGER)
    {
      error (0, 0, _("%s on device %s is not an integer control"),
             ctl->id, ctl->device->id);
      return -1;
    }
  return snd_ctl_elem_info_get_max (ctl->info);
}

long int
integer_ctl_min (const lk_device_ctl_t *ctl)
{
  int err = check_ctl_state (ctl);
  if (err)
    return err;
  if (snd_ctl_elem_info_get_type (ctl->info) != SND_CTL_ELEM_TYPE_INTEGER)
    {
      error (0, 0, _("%s on device %s is not an integer control"),
             ctl->id, ctl->device->id);
      return -1;
    }
  return snd_ctl_elem_info_get_min (ctl->info);
}

int
abs_in_max (const lk_device_t *device, unsigned int code)
{
  if (!device)
    {
      error (0, 0, _("Device does not exist"));
      return -1;
    }
  return libevdev_get_abs_maximum (device->input_dev, code);
}

int
abs_in_min (const lk_device_t *device, unsigned int code)
{
  if (!device)
    {
      error (0, 0, _("Device does not exist"));
      return -1;
    }
  return libevdev_get_abs_minimum (device->input_dev, code);
}

void
send_midi_keypress (const lk_device_t *device, unsigned char channel,
                    unsigned char note, unsigned char velocity)
{
  if (!device || !device->seq_handle)
    {
      error (0, 0, _("Device does not exist"));
      return;
    }
  snd_seq_event_t ev;
  snd_seq_ev_clear (&ev);
  snd_seq_ev_set_source (&ev, device->seq_read_port);
  snd_seq_ev_set_subs (&ev);
  snd_seq_ev_set_direct (&ev);

  snd_seq_ev_set_keypress (&ev, channel, note, velocity);

  snd_seq_event_output_direct (device->seq_handle, &ev);
  snd_seq_drain_output (device->seq_handle);
  snd_seq_free_event (&ev);
}

void
send_midi_note (const lk_device_t *device, unsigned char channel,
                unsigned char note, unsigned char velocity,
                unsigned int dur)
{
  if (!device || !device->seq_handle)
    {
      error (0, 0, _("Device does not exist"));
      return;
    }
  snd_seq_event_t ev;
  snd_seq_ev_clear (&ev);
  snd_seq_ev_set_source (&ev, device->seq_read_port);
  snd_seq_ev_set_subs (&ev);
  snd_seq_ev_set_direct (&ev);

  snd_seq_ev_set_note (&ev, channel, note, velocity, dur);

  snd_seq_event_output_direct (device->seq_handle, &ev);
  snd_seq_drain_output (device->seq_handle);
  snd_seq_free_event (&ev);
}

void
send_midi_noteon (const lk_device_t *device, unsigned char channel,
                  unsigned char note, unsigned char velocity)
{
  if (!device || !device->seq_handle)
    {
      error (0, 0, _("Device does not exist"));
      return;
    }
  snd_seq_event_t ev;
  snd_seq_ev_clear (&ev);
  snd_seq_ev_set_source (&ev, device->seq_read_port);
  snd_seq_ev_set_subs (&ev);
  snd_seq_ev_set_direct (&ev);

  snd_seq_ev_set_noteon (&ev, channel, note, velocity);

  snd_seq_event_output_direct (device->seq_handle, &ev);
  snd_seq_drain_output (device->seq_handle);
  snd_seq_free_event (&ev);
}

void
send_midi_noteoff (const lk_device_t *device, unsigned char channel,
                   unsigned char note, unsigned char velocity)
{
  if (!device || !device->seq_handle)
    {
      error (0, 0, _("Device does not exist"));
      return;
    }
  snd_seq_event_t ev;
  if (!device || !device->seq_handle)  snd_seq_ev_clear (&ev);
  snd_seq_ev_set_source (&ev, device->seq_read_port);
  snd_seq_ev_set_subs (&ev);
  snd_seq_ev_set_direct (&ev);

  snd_seq_ev_set_noteoff (&ev, channel, note, velocity);

  snd_seq_event_output_direct (device->seq_handle, &ev);
  snd_seq_drain_output (device->seq_handle);
  snd_seq_free_event (&ev);
}

void
send_midi_pgmchange (const lk_device_t *device, unsigned char channel,
                     signed int value)
{
  if (!device || !device->seq_handle)
    {
      error (0, 0, _("Device does not exist"));
      return;
    }
  snd_seq_event_t ev;
  snd_seq_ev_clear (&ev);
  snd_seq_ev_set_source (&ev, device->seq_read_port);
  snd_seq_ev_set_subs (&ev);
  snd_seq_ev_set_direct (&ev);

  snd_seq_ev_set_pgmchange (&ev, channel, value);

  snd_seq_event_output_direct (device->seq_handle, &ev);
  snd_seq_drain_output (device->seq_handle);
  snd_seq_free_event (&ev);
}

void
send_midi_pitchbend (const lk_device_t *device, unsigned char channel,
                     signed int value)
{
  if (!device || !device->seq_handle)
    {
      error (0, 0, _("Device does not exist"));
      return;
    }
  snd_seq_event_t ev;
  snd_seq_ev_clear (&ev);
  snd_seq_ev_set_source (&ev, device->seq_read_port);
  snd_seq_ev_set_subs (&ev);
  snd_seq_ev_set_direct (&ev);

  snd_seq_ev_set_pitchbend (&ev, channel, value);

  snd_seq_event_output_direct (device->seq_handle, &ev);
  snd_seq_drain_output (device->seq_handle);
  snd_seq_free_event (&ev);
}

void
send_midi_control (const lk_device_t *device, unsigned char channel,
                   unsigned int param, signed int value)
{
  if (!device || !device->seq_handle)
    {
      error (0, 0, _("Device does not exist"));
      return;
    }
  snd_seq_event_t ev;
  snd_seq_ev_clear (&ev);
  snd_seq_ev_set_source (&ev, device->seq_read_port);
  snd_seq_ev_set_subs (&ev);
  snd_seq_ev_set_direct (&ev);

  snd_seq_ev_set_controller (&ev, channel, param, value);

  snd_seq_event_output_direct (device->seq_handle, &ev);
  snd_seq_drain_output (device->seq_handle);
  snd_seq_free_event (&ev);
}

int
remap_enable_event (lk_device_t *device, unsigned int type, unsigned int code)
{
  if (!device->remap_dev)
    {
      error (0, 0, _("Cannot enable remap event because the remap device "
                     "has not been created."));
      return -1;
    }
  if (libevdev_has_event_code (device->remap_dev, type, code))
    return -1;
  if (!libevdev_has_event_type (device->remap_dev, type))
    {
      libevdev_enable_event_type (device->remap_dev, type);
    }
  if (type == EV_ABS)
    {
      struct input_absinfo absinfo = {0, 0, 1000, 0, 0, 1};
      return libevdev_enable_event_code (device->remap_dev, type, code, &absinfo);
    }
  return libevdev_enable_event_code (device->remap_dev, type, code, NULL);
}

int
remap_disable_event (lk_device_t *device, unsigned int type, unsigned int code)
{
  if (!device->remap_dev)
    {
      error (0, 0, _("Cannot disable remap event because the remap device "
                     "has not been created."));
      return -1;
    }
  if (!libevdev_has_event_code (device->remap_dev, type, code))
    return -1;
  return libevdev_disable_event_code (device->remap_dev, type, code);
}

int
remap_set_abs_info (lk_device_t *device, unsigned int code, int min, int max,
                     int resolution, int fuzz, int flat)
{
  if (!device->remap_dev)
    {
      error (0, 0, _("Cannot set remap event ABS info because the remap device "
                     "has not been created."));
      return -1;
    }
  if (!libevdev_has_event_type (device->remap_dev, EV_ABS))
    {
      error (0, 0, _("Cannot set remap event ABS info because ABS events have "
                     "not been enabled for the remap device."));
      return -1;
    }
  if (!libevdev_has_event_code (device->remap_dev, EV_ABS, code))
    {
      error (0, 0, _("Cannot set remap event ABS info because this ABS event "
                     "code has not been enabled for the remap device."));
      return -1;
    }
  struct input_absinfo absinfo = {min, min, max, fuzz, flat, resolution};
  libevdev_set_abs_info (device->remap_dev, code, &absinfo);
  return 0;
}

int
finalize_remap_dev (lk_device_t *device)
{
  int err = libevdev_uinput_create_from_device (device->remap_dev,
                                                LIBEVDEV_UINPUT_OPEN_MANAGED,
                                                &(device->uidev));
  if (err)
    {
      err = -err;
      error (0, err, _("Error finalizing remap device"));
    }
  return err;
}

int
send_remap_event (lk_device_t *device, unsigned int type, unsigned int code,
                   int value)
{
  if (!device->remap_dev || !device->uidev)
    {
      error (0, 0, _("Cannot send remap event because the remap device hasn't "
                     "been finalized yet."));
      return -1;
    }
  if (!libevdev_has_event_type (device->remap_dev, type) ||
      !libevdev_has_event_code (device->remap_dev, type, code))
    {
      error (0, 0, _("Cannot send remap event because event type %d code %d "
                     "hasn't been enabled for this remap device."), type, code);
      return -1;
    }
  int err = libevdev_uinput_write_event (device->uidev, type, code, value);
  if (err)
    {
      err = -err;
      error (0, err, _("Error sending remap event"));
      return err;
    }
  err = libevdev_uinput_write_event (device->uidev, EV_SYN, SYN_REPORT, 0);
  if (err)
    {
      err = -err;
      error (0, err, _("Error sending remap SYN event"));
      return err;
    }
  return 0;
}
