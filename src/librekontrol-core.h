/* 
 * librekontrol-core.h --- 
 * 
 * Copyright (C) 2016, 2019 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREKONTROL_CORE_H
#define LIBREKONTROL_CORE_H

#include <config.h>
#include <libguile.h>
#include <libevdev/libevdev.h>
#include <libintl.h>
#include "lk-device.h"

extern lk_device_t **lk_device_list;
extern size_t lk_num_devices;
extern lk_device_ctl_t **lk_ctl_list;
extern size_t lk_num_ctls;

void *init_lk_scm (void *data);
void handle_event (const char *device_id, int type, int code, int value);
void run_idle_hook (const char *device_id);
unsigned int get_idle_wait (const char *device_id);
void run_exit_hook (const char *device_id);
void lk_scm_cleanup (void);

#endif
