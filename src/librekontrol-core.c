/* 
 * librekontrol-core.c --- 
 * 
 * Copyright (C) 2016, 2017, 2019 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "librekontrol-core.h"

#define _(String) gettext (String)
#define gettext_noop(String) String
#define N_(String) gettext_noop (String)

static SCM device_type;
static SCM ctl_type;

lk_device_t **lk_device_list = NULL;
size_t lk_num_devices = 0;
lk_device_ctl_t **lk_ctl_list = NULL;
size_t lk_num_ctls = 0;

struct open_device_req
{
  char *ctl_name_str;
  char *in_name_str;
  bool open_midi_seq;
  bool open_output_evdev;
  int err;
};

struct open_ctl_req
{
  size_t device_n;
  char *ctl_id_str;
  int err;
};

struct bool_ctl_req
{
  size_t ctl_n;
  bool value;
};

struct int_ctl_req
{
  size_t ctl_n;
  long int value;
};

struct abs_in_req
{
  size_t device_n;
  unsigned int code;
  int value;
};

struct remap_toggle_event_req
{
  size_t device_n;
  unsigned int code;
  unsigned int type;
  bool enable;
  int err;
};

struct remap_set_abs_info_req
{
  size_t device_n;
  unsigned int code;
  int min;
  int max;
  int resolution;
  int fuzz;
  int flat;
  int err;
};

struct finalize_remap_dev_req
{
  size_t device_n;
  int err;
};

struct send_remap_event_req
{
  size_t device_n;
  unsigned int type;
  unsigned int code;
  int value;
  int err;
};

struct send_midi_req
{
  size_t device_n;
  unsigned char channel;
  unsigned char note;
  unsigned char velocity;
  unsigned int dur;
};

struct send_midi_param_req
{
  size_t device_n;
  unsigned char channel;
  unsigned int param;
  int value;
};

void*
open_device_wrapper (void *data)
{
  struct open_device_req *request = data;
  if (lk_num_devices == 0)
    {
      lk_device_list = (lk_device_t **)malloc ((++lk_num_devices)*sizeof (lk_device_t));
      lk_device_list[0] = (lk_device_t *)malloc (sizeof (lk_device_t));
    }
  else
    {
      lk_device_list = (lk_device_t **)realloc (lk_device_list, (++lk_num_devices)*sizeof (lk_device_t));
      lk_device_list[lk_num_devices-1] = (lk_device_t *)malloc (sizeof (lk_device_t));
    }
  if (!lk_device_list || !lk_device_list[lk_num_devices-1])
    {
      error (EXIT_FAILURE, errno, _("Memory exhausted"));
    }
  request->err = open_device (lk_device_list[lk_num_devices-1], request->ctl_name_str,
                              request->in_name_str, request->open_midi_seq,
                              request->open_output_evdev);
  if (request->err)
    {
      free (lk_device_list[lk_num_devices-1]);
      lk_num_devices--;
      if (lk_num_devices > 0)
        {
          lk_device_list = (lk_device_t **)realloc (lk_device_list, lk_num_devices*sizeof (lk_device_t));
          if (!lk_device_list)
            {
              error (EXIT_FAILURE, errno, _("Memory exhausted?"));
            }
        }
      else
        {
          free (lk_device_list);
          lk_device_list = NULL;
        }
    }
  return (void *)request;
}

void*
open_ctl_wrapper (void *data)
{
  struct open_ctl_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, "Invalid device selected");
    }
  lk_device_t *device = lk_device_list[request->device_n];
  if (lk_num_ctls == 0)
    {
      lk_ctl_list = (lk_device_ctl_t **)malloc ((++lk_num_ctls)*sizeof (lk_device_ctl_t));
      lk_ctl_list[0] = (lk_device_ctl_t *)malloc (sizeof (lk_device_ctl_t));
    }
  else
    {
      lk_ctl_list = (lk_device_ctl_t **)realloc (lk_ctl_list, (++lk_num_ctls)*sizeof (lk_device_ctl_t));
      lk_ctl_list[lk_num_ctls-1] = (lk_device_ctl_t *)malloc (sizeof (lk_device_ctl_t));
    }
  if (!lk_ctl_list || !lk_ctl_list[lk_num_ctls-1])
    {
      error (EXIT_FAILURE, errno, _("Memory exhausted"));
    }
  request->err = open_control (lk_ctl_list[lk_num_ctls-1], device,
                               request->ctl_id_str);
  if (request->err)
    {
      free (lk_ctl_list[lk_num_ctls-1]);
      lk_ctl_list[lk_num_ctls-1] = NULL;
    }
  return (void *)request;
}

void*
set_boolean_ctl_wrapper (void *data)
{
  struct bool_ctl_req *request = data;
  if (request->ctl_n >= lk_num_ctls)
    {
      error (EXIT_FAILURE, 0, _("Invalid ctl selected"));
    }
  lk_device_ctl_t *ctl = lk_ctl_list[request->ctl_n];
  set_boolean_ctl (ctl, request->value);
  return (void *)request;
}

void*
get_boolean_ctl_wrapper (void *data)
{
  struct bool_ctl_req *request = data;
  if (request->ctl_n >= lk_num_ctls)
    {
      error (EXIT_FAILURE, 0, _("Invalid ctl selected"));
    }
  lk_device_ctl_t *ctl = lk_ctl_list[request->ctl_n];
  if (ctl)
    request->value = get_boolean_ctl (ctl);
  return (void *)request;
}

void*
toggle_boolean_ctl_wrapper (void *data)
{
  struct bool_ctl_req *request = data;
  if (request->ctl_n >= lk_num_ctls)
    {
      error (EXIT_FAILURE, 0, _("Invalid ctl selected"));
    }
  lk_device_ctl_t *ctl = lk_ctl_list[request->ctl_n];
  toggle_boolean_ctl (ctl);
  return (void *)request;
}

void*
set_integer_ctl_wrapper (void *data)
{
  struct int_ctl_req *request = data;
  if (request->ctl_n >= lk_num_ctls)
    {
      error (EXIT_FAILURE, 0, _("Invalid ctl selected"));
    }
  lk_device_ctl_t *ctl = lk_ctl_list[request->ctl_n];
  set_integer_ctl (ctl, request->value);
  return (void *)request;
}

void*
get_integer_ctl_wrapper (void *data)
{
  struct int_ctl_req *request = data;
  if (request->ctl_n >= lk_num_ctls)
    {
      error (EXIT_FAILURE, 0, _("Invalid ctl selected"));
    }
  lk_device_ctl_t *ctl = lk_ctl_list[request->ctl_n];
  request->value = get_integer_ctl (ctl);
  return (void *)request;
}

void*
integer_ctl_max_wrapper (void *data)
{
  struct int_ctl_req *request = data;
  if (request->ctl_n >= lk_num_ctls)
    {
      error (EXIT_FAILURE, 0, _("Invalid ctl selected"));
    }
  lk_device_ctl_t *ctl = lk_ctl_list[request->ctl_n];
  request->value = integer_ctl_max (ctl);
  return (void *)request;
}

void*
integer_ctl_min_wrapper (void *data)
{
  struct int_ctl_req *request = data;
  if (request->ctl_n >= lk_num_ctls)
    {
      error (EXIT_FAILURE, 0, _("Invalid ctl selected"));
    }
  lk_device_ctl_t *ctl = lk_ctl_list[request->ctl_n];
  request->value = integer_ctl_min (ctl);
  return (void *)request;
}

void*
abs_in_max_wrapper (void *data)
{
  struct abs_in_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  request->value = abs_in_max (device, request->code);
  return (void *)request;
}

void*
abs_in_min_wrapper (void *data)
{
  struct abs_in_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  request->value = abs_in_min (device, request->code);
  return (void *)request;
}

void*
remap_toggle_event_wrapper (void *data)
{
  struct remap_toggle_event_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  if (request->enable)
    {
      request->err = remap_enable_event (device, request->type, request->code);
    }
  else
    {
      request->err = remap_disable_event (device, request->type, request->code);
    }
  return (void *)request;
}

void*
remap_set_abs_info_wrapper (void *data)
{
  struct remap_set_abs_info_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  request->err = remap_set_abs_info (device, request->code, request->min,
                                     request->max, request->resolution,
                                     request->fuzz, request->flat);
  return (void *)request;
}

void*
finalize_remap_dev_wrapper (void *data)
{
  struct finalize_remap_dev_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  request->err = finalize_remap_dev (device);
  return (void *)request;
}

void*
send_remap_event_wrapper (void *data)
{
  struct send_remap_event_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  request->err = send_remap_event (device, request->type, request->code,
                                   request->value);
  return (void *)request;
}

void*
send_midi_keypress_wrapper (void *data)
{
  struct send_midi_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  send_midi_keypress (device, request->channel, request->note, request->velocity);
}

void*
send_midi_note_wrapper (void *data)
{
  struct send_midi_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  send_midi_note (device, request->channel, request->note, request->velocity,
                      request->dur);
}

void*
send_midi_noteon_wrapper (void *data)
{
  struct send_midi_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  send_midi_noteon (device, request->channel, request->note, request->velocity);
}

void*
send_midi_noteoff_wrapper (void *data)
{
  struct send_midi_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  send_midi_noteoff (device, request->channel, request->note, request->velocity);
}

void*
send_midi_pgmchange_wrapper (void *data)
{
  struct send_midi_param_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  send_midi_pgmchange (device, request->channel, request->value);
}

void*
send_midi_pitchbend_wrapper (void *data)
{
  struct send_midi_param_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  send_midi_pitchbend (device, request->channel, request->value);
}

void*
send_midi_control_wrapper (void *data)
{
  struct send_midi_param_req *request = data;
  if (request->device_n >= lk_num_devices)
    {
      error (EXIT_FAILURE, 0, _("Invalid device selected"));
    }
  lk_device_t *device = lk_device_list[request->device_n];
  send_midi_control (device, request->channel, request->param, request->value);
}

SCM
lk_scm_set_idle_wait (SCM lk_device_s, SCM idle_wait)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to set idle wait: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  size_t device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0);
  lk_device_t *device = lk_device_list[device_n];
  if (!device)
    {
      error (EXIT_FAILURE, 0, _("Lost track of a device, bailing out."));
    }
  SCM s_dev_id = scm_from_latin1_string (device->id);
  SCM s_idle_waits_symbol = scm_c_lookup ("lk-idle-waits");
  SCM s_idle_waits = scm_variable_ref (s_idle_waits_symbol);
  scm_hash_set_x (s_idle_waits, s_dev_id, idle_wait);
  return SCM_BOOL_T;
}

void
init_device_type (void)
{
  SCM name = scm_from_latin1_symbol ("lk-device");
  SCM slots = scm_list_1 (scm_from_latin1_symbol ("lk-device-number"));
  scm_t_struct_finalize finalizer = NULL;
  device_type = scm_make_foreign_object_type (name, slots, finalizer);
}

void
init_ctl_type (void)
{
  SCM name = scm_from_latin1_symbol ("lk-ctl");
  SCM slots = scm_list_2 (scm_from_latin1_symbol ("lk-device-number"),
                          scm_from_latin1_symbol ("lk-ctl-number"));
  scm_t_struct_finalize finalizer = NULL;

  ctl_type = scm_make_foreign_object_type (name, slots, finalizer);
}

SCM
lk_scm_open_device (SCM controller_name, SCM input_name, SCM open_midi_seq,
                       SCM open_output_evdev)
{
  char *ctl_name_str = NULL;
  char *in_name_str = NULL;
  if (!scm_is_false (controller_name))
    {
      ctl_name_str = scm_to_latin1_stringn (controller_name, NULL);
    }
  if (!scm_is_false (input_name))
    {
      in_name_str = scm_to_latin1_stringn (input_name, NULL);
    }
  struct open_device_req request = {.ctl_name_str = ctl_name_str,
                                    .in_name_str = in_name_str,
                                    .open_midi_seq = scm_to_bool (open_midi_seq),
                                    .open_output_evdev = scm_to_bool (open_output_evdev),
                                    .err = 0};
  scm_without_guile (&open_device_wrapper, &request);
  if (request.err < 0)
    {
      return SCM_BOOL_F;
    }
  SCM lk_device_s = scm_make_foreign_object_0 (device_type);
  scm_foreign_object_unsigned_set_x (lk_device_s, 0, lk_num_devices-1);
  scm_permanent_object (lk_device_s);
  lk_scm_set_idle_wait (lk_device_s, scm_from_double(1.0));

  SCM s_ev_hand_symbol = scm_c_lookup ("lk-event-handlers");
  SCM s_event_handlers = scm_variable_ref (s_ev_hand_symbol);
  SCM s_ev_hand_key = scm_from_latin1_string (lk_device_list[lk_num_devices-1]->id);
  scm_hash_set_x (s_event_handlers, s_ev_hand_key, scm_c_make_hash_table (1));
  return lk_device_s;
}

SCM
lk_scm_open_ctl (SCM lk_device_s, SCM ctl_id_num)
{
  char *ctl_id_str;
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to open control %s: device not opened."),
             ctl_id_str);
      return SCM_BOOL_F;
    }
  SCM ctl_id = scm_simple_format (SCM_BOOL_F, scm_from_latin1_string ("numid=~a"),
                                  scm_list_1 (ctl_id_num));
  ctl_id_str = scm_to_latin1_stringn (ctl_id, NULL);
  scm_assert_foreign_object_type (device_type, lk_device_s);
  size_t device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0);
  struct open_ctl_req request = {.device_n = device_n,
                                 .ctl_id_str = ctl_id_str,
                                 .err = 0};
  scm_without_guile (&open_ctl_wrapper, &request);
  if (request.err < 0)
    {
      return SCM_BOOL_F;
    }
  SCM lk_ctl_s = scm_make_foreign_object_0 (ctl_type);
  scm_foreign_object_unsigned_set_x (lk_ctl_s, 0, device_n);
  scm_foreign_object_unsigned_set_x (lk_ctl_s, 1, lk_num_ctls-1);
  scm_permanent_object (lk_ctl_s);
  return lk_ctl_s;
}

SCM
lk_scm_set_boolean_ctl (SCM lk_ctl_s, SCM value)
{
  if (scm_is_false (lk_ctl_s))
    {
      error (0, 0, _("Failed to set control: control not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (ctl_type, lk_ctl_s);
  size_t ctl_n = scm_foreign_object_unsigned_ref (lk_ctl_s, 1);
  struct bool_ctl_req request = {.ctl_n = ctl_n,
                                 .value = (bool) scm_to_bool (value)};
  scm_without_guile (&set_boolean_ctl_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_get_boolean_ctl (SCM lk_ctl_s)
{
  if (scm_is_false (lk_ctl_s))
    {
      error (0, 0, _("Failed to get control: control not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (ctl_type, lk_ctl_s);
  size_t ctl_n = scm_foreign_object_unsigned_ref (lk_ctl_s, 1);
  struct bool_ctl_req request = {.ctl_n = ctl_n};
  scm_without_guile (&get_boolean_ctl_wrapper, &request);
  return scm_from_bool ((int) request.value);
}

SCM
lk_scm_toggle_boolean_ctl (SCM lk_ctl_s)
{
  if (scm_is_false (lk_ctl_s))
    {
      error (0, 0, _("Failed to toggle control: control not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (ctl_type, lk_ctl_s);
  size_t ctl_n = scm_foreign_object_unsigned_ref (lk_ctl_s, 1);
  struct bool_ctl_req request = {.ctl_n = ctl_n};
  scm_without_guile (&toggle_boolean_ctl_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_set_integer_ctl (SCM lk_ctl_s, SCM value)
{
  if (scm_is_false (lk_ctl_s))
    {
      error (0, 0, _("Failed to set control: control not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (ctl_type, lk_ctl_s);
  size_t ctl_n = scm_foreign_object_unsigned_ref (lk_ctl_s, 1);
  struct int_ctl_req request = {.ctl_n = ctl_n,
                                .value = scm_to_long (value)};
  scm_without_guile (&set_integer_ctl_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_get_integer_ctl (SCM lk_ctl_s)
{
  if (scm_is_false (lk_ctl_s))
    {
      error (0, 0, _("Failed to get control: control not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (ctl_type, lk_ctl_s);
  size_t ctl_n = scm_foreign_object_unsigned_ref (lk_ctl_s, 1);
  struct int_ctl_req request = {.ctl_n = ctl_n};
  scm_without_guile (&get_integer_ctl_wrapper, &request);
  return scm_from_long (request.value);
}

SCM
lk_scm_integer_ctl_max (SCM lk_ctl_s)
{
  if (scm_is_false (lk_ctl_s))
    {
      error (0, 0, _("Failed to get control max: control not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (ctl_type, lk_ctl_s);
  size_t ctl_n = scm_foreign_object_unsigned_ref (lk_ctl_s, 1);
  struct int_ctl_req request = {.ctl_n = ctl_n};
  scm_without_guile (&integer_ctl_max_wrapper, &request);
  return scm_from_long (request.value);
}

SCM
lk_scm_integer_ctl_min (SCM lk_ctl_s)
{
  if (scm_is_false (lk_ctl_s))
    {
      error (0, 0, _("Failed to get control min: control not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (ctl_type, lk_ctl_s);
  size_t ctl_n = scm_foreign_object_unsigned_ref (lk_ctl_s, 1);
  struct int_ctl_req request = {.ctl_n = ctl_n};
  scm_without_guile (&integer_ctl_min_wrapper, &request);
  return scm_from_long (request.value);
}

SCM
lk_scm_abs_in_max (SCM lk_device_s, SCM in_code)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to get ABS max: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  size_t device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0);
  struct abs_in_req request = {.device_n = device_n,
                               .code = scm_to_uint (in_code)};
  scm_without_guile (&abs_in_max_wrapper, &request);
  return scm_from_int (request.value);
}

SCM
lk_scm_abs_in_min (SCM lk_device_s, SCM in_code)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to get ABS max: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  size_t device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0);
  struct abs_in_req request = {.device_n = device_n,
                               .code = scm_to_uint (in_code)};
  scm_without_guile (&abs_in_min_wrapper, &request);
  return scm_from_int (request.value);
}

SCM
lk_scm_send_midi_keypress (SCM lk_device_s, SCM channel, SCM note, SCM velocity)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send midi: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_midi_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                  .channel = scm_to_uchar (channel),
                                  .note = scm_to_uchar (note),
                                  .velocity = scm_to_uchar (velocity)};
  scm_without_guile (&send_midi_keypress_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_send_midi_note (SCM lk_device_s, SCM channel, SCM note, SCM velocity,
                       SCM dur)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send midi: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_midi_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                  .channel = scm_to_uchar (channel),
                                  .note = scm_to_uchar (note),
                                  .velocity = scm_to_uchar (velocity),
                                  .dur = scm_to_uint (dur)};
  scm_without_guile (&send_midi_note_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_send_midi_noteon (SCM lk_device_s, SCM channel, SCM note, SCM velocity)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send midi: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_midi_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                  .channel = scm_to_uchar (channel),
                                  .note = scm_to_uchar (note),
                                  .velocity = scm_to_uchar (velocity)};
  scm_without_guile (&send_midi_noteon_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_send_midi_noteoff (SCM lk_device_s, SCM channel, SCM note, SCM velocity)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send midi: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_midi_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                  .channel = scm_to_uchar (channel),
                                  .note = scm_to_uchar (note),
                                  .velocity = scm_to_uchar (velocity)};
  scm_without_guile (&send_midi_noteoff_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_send_midi_pgmchange (SCM lk_device_s, SCM channel, SCM value)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send midi: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_midi_param_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                        .channel = scm_to_uchar (channel),
                                        .value = scm_to_int (value)};
  scm_without_guile (&send_midi_pgmchange_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_send_midi_pitchbend (SCM lk_device_s, SCM channel, SCM value)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send midi: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_midi_param_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                        .channel = scm_to_uchar (channel),
                                        .value = scm_to_int (value)};
  scm_without_guile (&send_midi_pitchbend_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_send_midi_control (SCM lk_device_s, SCM channel, SCM param, SCM value)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send midi: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_midi_param_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                        .channel = scm_to_uchar (channel),
                                        .param = scm_to_uint (param),
                                        .value = scm_to_int (value)};
  scm_without_guile (&send_midi_control_wrapper, &request);
  return SCM_BOOL_T;
}

SCM
lk_scm_remap_enable_event (SCM lk_device_s, SCM type, SCM code)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to enable remap event: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct remap_toggle_event_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                           .type = scm_to_uint (type),
                                           .code = scm_to_uint (code),
                                           .enable = true};
  scm_without_guile (&remap_toggle_event_wrapper, &request);
  return scm_from_int (request.err);
}

SCM
lk_scm_remap_disable_event (SCM lk_device_s, SCM type, SCM code)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to disable remap event: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct remap_toggle_event_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                           .type = scm_to_uint (type),
                                           .code = scm_to_uint (code),
                                           .enable = false};
  scm_without_guile (&remap_toggle_event_wrapper, &request);
  return scm_from_int (request.err);
}

SCM
lk_scm_remap_set_abs_info (SCM lk_device_s, SCM code, SCM min, SCM max,
                           SCM resolution, SCM fuzz, SCM flat)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to set remap abs info: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct remap_set_abs_info_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                           .code = scm_to_uint (code),
                                           .min = scm_to_int (min),
                                           .max = scm_to_int (max),
                                           .resolution = scm_to_int (resolution),
                                           .fuzz = scm_to_int (fuzz),
                                           .flat = scm_to_int (flat)};
  scm_without_guile (&remap_set_abs_info_wrapper, &request);
  return scm_from_int (request.err);
}

SCM
lk_scm_finalize_remap_dev (SCM lk_device_s)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to finalize remap device: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct finalize_remap_dev_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0)};
  scm_without_guile (&finalize_remap_dev_wrapper, &request);
  return scm_from_int (request.err);
}

SCM
lk_scm_send_remap_event (SCM lk_device_s, SCM type, SCM code, SCM value)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to send remap event: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  struct send_remap_event_req request = {.device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0),
                                         .type = scm_to_uint (type),
                                         .code = scm_to_uint (code),
                                         .value = scm_to_int (value)};
  scm_without_guile (&send_remap_event_wrapper, &request);
  return scm_from_int (request.err);
}

SCM
get_event_handler (SCM lk_device_s, SCM ev_id, bool create)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to map event: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  size_t device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0);
  lk_device_t *device = lk_device_list[device_n];
  SCM s_dev_id = scm_from_latin1_string (device->id);
  if (scm_is_false (scm_pair_p (ev_id)))
    {
      error (0, 0, _("The event ID must be a pair, e.g. '(1 . 2)."));
      return SCM_BOOL_F;
    }
  if (scm_is_false (scm_integer_p (scm_car (ev_id))))
    {
      error (0, 0, _("The event type must be an integer."));
      return SCM_BOOL_F;
    }
  if (scm_is_false (scm_integer_p (scm_cdr (ev_id))))
    {
      error (0, 0, _("The event code must be an integer."));
      return SCM_BOOL_F;
    }
  SCM s_ev_hand_symbol = scm_c_lookup ("lk-event-handlers");
  SCM s_event_handlers = scm_variable_ref (s_ev_hand_symbol);
  SCM s_dev_handlers = scm_hash_ref (s_event_handlers, s_dev_id, SCM_UNDEFINED);
  if (scm_is_false (s_dev_handlers))
    {
      error (EXIT_FAILURE, 0, _("Device event-handler list not found"));
    }
  SCM s_ev_handler = scm_hash_ref (s_dev_handlers, ev_id, SCM_UNDEFINED);
  if (scm_is_false (s_ev_handler) && create)
    {
      s_ev_handler = scm_make_hook (scm_from_int(1));
      scm_hash_set_x (s_dev_handlers, ev_id, s_ev_handler);
    }
  else
    {
      error (0, 0, _("No event handler found"));
      return SCM_BOOL_F;
    }
  return s_ev_handler;
}

SCM
lk_scm_add_event_hook (SCM lk_device_s, SCM ev_id, SCM proc)
{
  SCM s_ev_handler = get_event_handler (lk_device_s, ev_id, true);
  scm_add_hook_x (s_ev_handler, proc, SCM_BOOL_T);
  return SCM_BOOL_T;
}

SCM
lk_scm_remove_event_hook (SCM lk_device_s, SCM ev_id, SCM proc)
{
  SCM s_ev_handler = get_event_handler (lk_device_s, ev_id, false);
  scm_remove_hook_x (s_ev_handler, proc);
  return SCM_BOOL_T;
}

SCM
lk_scm_reset_event_hook (SCM lk_device_s, SCM ev_id)
{
  SCM s_ev_handler = get_event_handler (lk_device_s, ev_id, false);
  scm_reset_hook_x (s_ev_handler);
  return SCM_BOOL_T;
}

void
handle_event (const char *device_id, int type, int code, int value)
{
  SCM s_type = scm_from_int (type);
  SCM s_code = scm_from_int (code);
  SCM s_value = scm_from_int (value);
  SCM s_dev_id = scm_from_latin1_string (device_id);
  SCM s_ev_pair = scm_cons (s_type, s_code);
  SCM s_ev_hand_symbol = scm_c_lookup ("lk-event-handlers");
  SCM s_event_handlers = scm_variable_ref (s_ev_hand_symbol);
  SCM s_dev_handlers = scm_hash_ref (s_event_handlers, s_dev_id, SCM_UNDEFINED);
  if (scm_is_false (s_dev_handlers))
    {
      error (EXIT_FAILURE, 0, _("Device event-handler list not found"));
    }
  SCM s_ev_handler = scm_hash_ref (s_dev_handlers, s_ev_pair, SCM_UNDEFINED);
  if (scm_is_false (s_ev_handler))
    {
      error (0, 0, _("No event-handler for event: '(%d . %d)"), type, code);
      return;
    }
  scm_c_run_hook (s_ev_handler, scm_list_1(s_value));
}

SCM
get_idle_handler (SCM lk_device_s, bool create)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to map event: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  size_t device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0);
  lk_device_t *device = lk_device_list[device_n];
  SCM s_dev_id = scm_from_latin1_string (device->id);
  SCM s_idle_hand_symbol = scm_c_lookup ("lk-idle-handlers");
  SCM s_idle_handlers = scm_variable_ref (s_idle_hand_symbol);
  SCM s_dev_handler = scm_hash_ref (s_idle_handlers, s_dev_id, SCM_UNDEFINED);
  if (scm_is_false (s_dev_handler))
    {
      if (create)
        {
          s_dev_handler = scm_make_hook (scm_from_int(0));
          scm_hash_set_x (s_idle_handlers, s_dev_id, s_dev_handler);
        }
      else
        {
          error (0, 0, _("No idle-handler for device: %s"), device->str_id);
          return SCM_BOOL_F;
        }
    }
  return s_dev_handler;
}

SCM
lk_scm_add_idle_hook (SCM lk_device_s, SCM proc)
{
  SCM s_idle_handler = get_idle_handler (lk_device_s, true);
  scm_add_hook_x (s_idle_handler, proc, SCM_BOOL_T);
  return SCM_BOOL_T;
}

SCM
lk_scm_remove_idle_hook (SCM lk_device_s, SCM proc)
{
  SCM s_idle_handler = get_idle_handler (lk_device_s, false);
  scm_remove_hook_x (s_idle_handler, proc);
  return SCM_BOOL_T;
}

SCM
lk_scm_reset_idle_hook (SCM lk_device_s)
{
  SCM s_idle_handler = get_idle_handler (lk_device_s, false);
  scm_reset_hook_x (s_idle_handler);
  return SCM_BOOL_T;
}

void
run_idle_hook (const char *device_id)
{
  SCM s_dev_id = scm_from_latin1_string (device_id);
  SCM s_idle_hand_symbol = scm_c_lookup ("lk-idle-handlers");
  SCM s_idle_handlers = scm_variable_ref (s_idle_hand_symbol);
  SCM s_dev_handler = scm_hash_ref (s_idle_handlers, s_dev_id, SCM_UNDEFINED);
  if (!scm_is_false (s_dev_handler))
    {
      scm_c_run_hook (s_dev_handler, scm_list_n (SCM_UNDEFINED));
    }
}

unsigned int
get_idle_wait (const char *device_id)
{
  if (!device_id)
    {
      return 0;
    }
  SCM s_dev_id = scm_from_latin1_string (device_id);
  SCM s_idle_waits_symbol = scm_c_lookup ("lk-idle-waits");
  SCM s_idle_waits = scm_variable_ref (s_idle_waits_symbol);
  SCM s_dev_idle_wait = scm_hash_ref (s_idle_waits, s_dev_id, SCM_UNDEFINED);
  if (!scm_is_false (s_dev_idle_wait))
    {
      return (unsigned int) (scm_to_double (s_dev_idle_wait) * 1e6);
    }
  else
    {
      return 0;
    }
}

SCM
get_exit_handler (SCM lk_device_s, bool create)
{
  if (scm_is_false (lk_device_s))
    {
      error (0, 0, _("Failed to map event: device not opened."));
      return SCM_BOOL_F;
    }
  scm_assert_foreign_object_type (device_type, lk_device_s);
  size_t device_n = scm_foreign_object_unsigned_ref (lk_device_s, 0);
  lk_device_t *device = lk_device_list[device_n];
  SCM s_dev_id = scm_from_latin1_string (device->id);
  SCM s_exit_hand_symbol = scm_c_lookup ("lk-exit-handlers");
  SCM s_exit_handlers = scm_variable_ref (s_exit_hand_symbol);
  SCM s_dev_handler = scm_hash_ref (s_exit_handlers, s_dev_id, SCM_UNDEFINED);
  if (scm_is_false (s_dev_handler))
    {
      if (create)
        {
          s_dev_handler = scm_make_hook (scm_from_int(0));
          scm_hash_set_x (s_exit_handlers, s_dev_id, s_dev_handler);
        }
      else
        {
          error (0, 0, _("No exit-handler for device: %s"), device->str_id);
          return SCM_BOOL_F;
        }
    }
  return s_dev_handler;
}

SCM
lk_scm_add_exit_hook (SCM lk_device_s, SCM proc)
{
  SCM s_exit_handler = get_exit_handler (lk_device_s, true);
  scm_add_hook_x (s_exit_handler, proc, SCM_BOOL_T);
  return SCM_BOOL_T;
}

SCM
lk_scm_remove_exit_hook (SCM lk_device_s, SCM proc)
{
  SCM s_exit_handler = get_exit_handler (lk_device_s, false);
  scm_remove_hook_x (s_exit_handler, proc);
  return SCM_BOOL_T;
}

SCM
lk_scm_reset_exit_hook (SCM lk_device_s)
{
  SCM s_exit_handler = get_exit_handler (lk_device_s, false);
  scm_reset_hook_x (s_exit_handler);
  return SCM_BOOL_T;
}

void
run_exit_hook (const char *device_id)
{
  SCM s_dev_id = scm_from_latin1_string (device_id);
  SCM s_exit_hand_symbol = scm_c_lookup ("lk-exit-handlers");
  SCM s_exit_handlers = scm_variable_ref (s_exit_hand_symbol);
  SCM s_dev_handler = scm_hash_ref (s_exit_handlers, s_dev_id, SCM_UNDEFINED);
  if (!scm_is_false (s_dev_handler))
    {
      scm_c_run_hook (s_dev_handler, scm_list_n (SCM_UNDEFINED));
    }
}

void
init_lk_core_module (void *data)
{
  init_device_type ();
  init_ctl_type ();

  scm_c_define_gsubr ("open-device", 4, 0, 0, lk_scm_open_device);
  scm_c_define_gsubr ("open-ctl", 2, 0, 0, lk_scm_open_ctl);
  scm_c_export ("open-device", "open-ctl", NULL);

  scm_c_define_gsubr ("set-boolean-ctl", 2, 0, 0, lk_scm_set_boolean_ctl);
  scm_c_define_gsubr ("get-boolean-ctl", 1, 0, 0, lk_scm_get_boolean_ctl);
  scm_c_define_gsubr ("toggle-boolean-ctl", 1, 0, 0, lk_scm_toggle_boolean_ctl);
  scm_c_define_gsubr ("set-integer-ctl", 2, 0, 0, lk_scm_set_integer_ctl);
  scm_c_define_gsubr ("get-integer-ctl", 1, 0, 0, lk_scm_get_integer_ctl);
  scm_c_define_gsubr ("integer-ctl-max", 1, 0, 0, lk_scm_integer_ctl_max);
  scm_c_define_gsubr ("integer-ctl-min", 1, 0, 0, lk_scm_integer_ctl_min);
  scm_c_define_gsubr ("abs-input-max", 2, 0, 0, lk_scm_abs_in_max);
  scm_c_define_gsubr ("abs-input-min", 2, 0, 0, lk_scm_abs_in_min);
  scm_c_export ("set-boolean-ctl", "get-boolean-ctl", "toggle-boolean-ctl",
                "set-integer-ctl", "get-integer-ctl", "integer-ctl-max",
                "integer-ctl-min", "abs-input-max", "abs-input-min", NULL);

  scm_c_define_gsubr ("send-midi-keypress", 4, 0, 0, lk_scm_send_midi_keypress);
  scm_c_define_gsubr ("send-midi-note", 5, 0, 0, lk_scm_send_midi_note);
  scm_c_define_gsubr ("send-midi-noteon", 4, 0, 0, lk_scm_send_midi_noteon);
  scm_c_define_gsubr ("send-midi-noteoff", 4, 0, 0, lk_scm_send_midi_noteoff);
  scm_c_define_gsubr ("send-midi-pgmchange", 3, 0, 0, lk_scm_send_midi_pgmchange);
  scm_c_define_gsubr ("send-midi-pitchbend", 3, 0, 0, lk_scm_send_midi_pitchbend);
  scm_c_define_gsubr ("send-midi-control", 4, 0, 0, lk_scm_send_midi_control);
  scm_c_export ("send-midi-keypress", "send-midi-note", "send-midi-noteon",
                "send-midi-noteoff", "send-midi-pgmchange", "send-midi-pichbend",
                "send-midi-control", NULL);

  scm_c_define_gsubr ("remap-enable-event", 3, 0, 0, lk_scm_remap_enable_event);
  scm_c_define_gsubr ("remap-disable-event", 3, 0, 0, lk_scm_remap_disable_event);
  scm_c_define_gsubr ("remap-set-abs-info", 7, 0, 0, lk_scm_remap_set_abs_info);
  scm_c_define_gsubr ("finalize-remap-dev", 1, 0, 0, lk_scm_finalize_remap_dev);
  scm_c_define_gsubr ("send-remap-event", 4, 0, 0, lk_scm_send_remap_event);
  scm_c_export ("remap-enable-event", "remap-disable-event", "remap-set-abs-info",
                "finalize-remap-dev", "send-remap-event", NULL);

  scm_c_define_gsubr ("add-event-hook!", 3, 0, 0, lk_scm_add_event_hook);
  scm_c_define_gsubr ("remove-event-hook!", 3, 0, 0, lk_scm_remove_event_hook);
  scm_c_define_gsubr ("reset-event-hook!", 3, 0, 0, lk_scm_reset_event_hook);
  scm_c_define_gsubr ("add-idle-hook!", 2, 0, 0, lk_scm_add_idle_hook);
  scm_c_define_gsubr ("remove-idle-hook!", 2, 0, 0, lk_scm_remove_idle_hook);
  scm_c_define_gsubr ("reset-idle-hook!", 1, 0, 0, lk_scm_reset_idle_hook);
  scm_c_define_gsubr ("set-idle-wait!", 2, 0, 0, lk_scm_set_idle_wait);
  scm_c_define_gsubr ("add-exit-hook!", 2, 0, 0, lk_scm_add_exit_hook);
  scm_c_define_gsubr ("remove-exit-hook!", 2, 0, 0, lk_scm_remove_exit_hook);
  scm_c_define_gsubr ("reset-exit-hook!", 1, 0, 0, lk_scm_reset_exit_hook);
  scm_c_export ("add-event-hook!", "remove-event-hook!", "reset-event-hook!",
                "add-idle-hook!", "remove-idle-hook!", "reset-idle-hook!",
                "set-idle-wait!", "add-exit-hook!", "remove-exit-hook!",
                "reset-exit-hook!", NULL);

  scm_permanent_object
    (scm_c_define ("lk-device-list", scm_list_n (SCM_UNDEFINED)));
  scm_permanent_object
    (scm_c_define ("lk-event-handlers", scm_c_make_hash_table (1)));
  scm_permanent_object
    (scm_c_define ("lk-idle-handlers", scm_c_make_hash_table (1)));
  scm_permanent_object
    (scm_c_define ("lk-idle-waits", scm_c_make_hash_table (1)));
  scm_permanent_object
    (scm_c_define ("lk-exit-handlers", scm_c_make_hash_table (1)));
  scm_c_export ("lk-device-list", "lk-event-handlers", "lk-idle-handlers",
                "lk-idle-waits", "lk-exit-handlers", NULL);
}

void*
init_lk_scm (void *data)
{
  scm_c_define_module ("librekontrol core", &init_lk_core_module, data);
}

void
lk_scm_cleanup (void)
{
  for (size_t i=0; i<lk_num_ctls; i++)
    {
      if (!lk_ctl_list[i])
        continue;
      if (close_control (lk_ctl_list[i]))
        error (0, 0, _("Error closing ctl"));
      free (lk_ctl_list[i]);
    }
  if (lk_ctl_list)
    free (lk_ctl_list);
  for (size_t i=0; i<lk_num_devices; i++)
    {
      if (!lk_device_list[i])
        continue;
      if (close_device (lk_device_list[i]))
        error (0, 0, _("Error closing device"));
      free (lk_device_list[i]);
    }
  if (lk_device_list)
    free (lk_device_list);
}
